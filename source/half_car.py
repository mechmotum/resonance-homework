import sympy as _sm
import numpy as _np
from scipy.optimize import fsolve as _fsolve
import matplotlib.pyplot as _plt
from matplotlib.patches import Rectangle as _Rectangle
from matplotlib.patches import Ellipse as _Ellipse
from resonance.nonlinear_systems import MultiDoFNonLinearSystem
from resonance.linear_systems import MultiDoFLinearSystem

VIEW_WIDTH = 10


def derive_equations_of_motion():
    """Returns the symbolic nonlinear equations of motion along with lists of
    all the variables."""

    ## Constants ##

    # inertias
    mR, mF, mC, IC = _sm.symbols('m_R m_F m_C I_C', real=True, positive=True)
    # tire/suspension params
    kT, cT, ksR, csR, ksF, csF = _sm.symbols(
        'k_T c_T k_{Rs} c_{Rs} k_{Fs} c_{Fs}', real=True, positive=True)
    # geometry and other
    a, b, l, dw, lso, g = _sm.symbols('a b l d_w l_{so} g', real=True,
                                     positive=True)

    constants = [mR, mF, mC, IC,
                 kT, cT, ksR, csR, ksF, csF,
                 a, b, l, dw, lso, g]

    ## Coordinates and Speeds ##

    t = _sm.symbols('t')

    # coordinates
    theta = _sm.Function('theta')(t)  # vehicle pitch angle
    yC = _sm.Function('y_C')(t)  # vehicle mass center vertical position
    yRu = _sm.Function('y_{Ru}')(t)  # rear unsprung vertical position
    yFu = _sm.Function('y_{Fu}')(t)  # front unsprung vertical position

    # speeds
    omega = _sm.Function('omega')(t)
    vC = _sm.Function('v_C')(t)
    vRu = _sm.Function('v_{Ru}')(t)
    vFu = _sm.Function('v_{Fu}')(t)

    states = [theta, yC, yRu, yFu, omega, vC, vRu, vFu]

    # other helper coordinates
    yRs = yC - a*_sm.sin(theta) - b*_sm.cos(theta)  # rear car vertical position
    yFs = yRs + l*_sm.sin(theta)  # front car vertical position

    ## Inputs ##

    # road input
    yR = _sm.Function('y_R')(t)  # vertical height of road at the rear wheel
    yF = _sm.Function('y_F')(t)  # vertical height of road at the front wheel
    vR = _sm.Function('v_R')(t)
    vF = _sm.Function('v_F')(t)

    inputs = [yR, yF, vR, vF]

    ## Kinetic Energy ##

    T = (mR/2*yRu.diff(t)**2 +  # unsprung mass motion
         mF/2*yFu.diff(t)**2 +  # unspring mass motion
         mC/2*yC.diff(t)**2 +  # sprung mass motion
         IC/2*theta.diff(t)**2)  # spring mass rotation

    ## Potential Energy ##

    U = (mR*g*yRu + mF*g*yFu + mC*g*yC +  # gravity
         kT/2*(yRu-yR-dw/2)**2 +  # rear tire spring
         kT/2*(yFu-yF-dw/2)**2 +  # front tire spring
         ksR/2*(yRs-yRu-lso)**2 +  # rear suspension spring
         ksF/2*(yFs-yFu-lso)**2)  # front suspension spring

    ## Dissipative Energy ##

    R = (cT/2*(yRu.diff(t)-yR.diff(t))**2 +
         cT/2*(yFu.diff(t)-yF.diff(t))**2 +
         csR/2*(yRs.diff(t)-yRu.diff(t))**2 +
         csF/2*(yFs.diff(t)-yFu.diff(t))**2)

    ## Lagrange Equations ##

    def lagrange_eq(T, U, R, q):
        L = T - U
        return L.diff(q.diff(t)).diff(t) - L.diff(q) + R.diff(q.diff(t))

    f_theta = lagrange_eq(T, U, R, theta)
    f_yc = lagrange_eq(T, U, R, yC)
    f_yru = lagrange_eq(T, U, R, yRu)
    f_yfu = lagrange_eq(T, U, R, yFu)

    f = _sm.Matrix([f_theta, f_yc, f_yru, f_yfu])

    return f, states, inputs, constants, t


def first_order_form(f, states, inputs, constants, t):
    """Transforms the nonlinear Lagrange's equations into an explicit first
    order form that is a function of the state variables."""

    q = _sm.Matrix(states[:4])
    u = q.diff(t)

    M = f.jacobian(u.diff(t))
    F = (M * u.diff(t) - f).expand()  # expand will cancel some stuff
    udot = M.LUsolve(F)

    udot = udot.subs(dict(zip(u, states[4:])))
    udot = udot.subs(dict(zip([r.diff(t) for r in inputs[:2]], inputs[2:])))

    return udot


def lambdify(udot, states, inputs, constants):
    """Returns a tuple of functions that evaluate the right hand side of the
    ordinary differential equations."""

    funcs = []
    for udot_i in udot:
        funcs.append(_sm.lambdify(states + inputs + constants, udot_i))

    return tuple(funcs)


def generate_equilibrium_func(f, states, inputs, constants, t):
    """Returns a function that evaluates the nonlinear static force balance
    equations that can be used to find the equilibrium point.

    NOTE : This can probalby be solved symbolically but it is not trivial.

    """

    q = _sm.Matrix(states[:4])
    u = q.diff(t)
    r = _sm.Matrix(inputs[:2])
    rdot = r.diff(t)
    f_static = f.subs(dict(zip(u.diff(t), _sm.zeros(4, 1))))\
                .subs(dict(zip(u, _sm.zeros(4, 1))))\
                .subs(dict(zip(rdot, _sm.zeros(2, 1))))
    func = _sm.lambdify(states[:4] + inputs[:2] + constants, f_static)

    def wrapped_func(qs, *args):
        return func(*(list(qs) + list(args))).squeeze()

    return wrapped_func


def linearize_equations_of_motion(f, states, inputs, constants, t):
    """Returns the symbolic canonical matrices for the system. These are a
    function of the equilibrium coordinates."""
    q = _sm.Matrix(states[:4])
    u = q.diff(t)
    v = q.col_join(u).col_join(u.diff(t))
    eq_syms = list(_sm.symbols(
        r'\theta_{eq}, y_{c_{eq}}, y_{Ru_{eq}}, y_{Fu_{eq}}'))
    v0 = _sm.Matrix(eq_syms + 8 * [0])
    v_subs = dict(zip(v, v0))

    f_lin = f.subs(v_subs) + f.jacobian(v).subs(v_subs) * (v - v0)

    M = f_lin.jacobian(u.diff(t))
    C = f_lin.jacobian(u)
    K = f_lin.jacobian(q)
    F = M * u.diff(t) + C * u + K * q - f_lin

    return M, C, K, F, eq_syms


def lambdify_M_C_K(M, C, K, eq_syms, constants):
    eval_M = _sm.lambdify(constants + eq_syms, M)
    eval_C = _sm.lambdify(constants + eq_syms, C)
    eval_K = _sm.lambdify(constants + eq_syms, K)
    return eval_M, eval_C, eval_K


def plot_config(theta, yRs, yFs, yRu, yFu, xR, xF, yR, yF, b, l, dw, time):

    fig, ax = _plt.subplots(1, 1)

    ax.set_ylim((-1.0, 3.0))
    ax.set_ylabel('Height [m]')
    ax.set_xlabel('Distance [m]')
    ax.set_aspect('equal')

    lat = _np.linspace(xR - VIEW_WIDTH / 3, xR + 2 * VIEW_WIDTH / 2, num=100)

    ax.set_xlim((lat[0], lat[-1]))

    rect = _Rectangle((xR, yRs),  # (x,y)
                      l,  # width
                      b * 2,  # height
                      angle=_np.rad2deg(theta))

    front_wheel = _Ellipse((xF, yFu), dw, dw, color='red')
    rear_wheel = _Ellipse((xR, yRu), dw, dw, color='red')

    ax.add_patch(rect)
    ax.add_patch(front_wheel)
    ax.add_patch(rear_wheel)

    time_txt = ax.text(xR - 3.0, 2.5, 'Time: {:1.1f} s'.format(time))

    # NOTE : Plot a flat road, because the profile isn't available until the
    # system is simulated.
    road_R = ax.plot(lat, _np.zeros_like(lat), color='black')[0]
    road_F = ax.plot(lat, _np.zeros_like(lat), color='black')[0]

    rear_suspension = ax.plot([xR, xR],
                              [yRu, yRs],
                              linewidth=3, marker='o', color='yellow')[0]
    front_suspension = ax.plot([xF, xF],
                               [yFu, yFs],
                               linewidth=3, marker='o', color='yellow')[0]

    return (fig, ax, rect, road_R, road_F, rear_suspension, front_suspension,
            rear_wheel, front_wheel, time_txt)


def plot_update(theta, yRs, yFs, yRu, yFu,
                xR, xR__hist, xR__futr, xF, xF__hist, xF__futr,
                yR, yR__hist, yR__futr, yF, yF__hist, yF__futr,
                time,
                ax, rect, road_R, road_F, rear_suspension, front_suspension,
                rear_wheel, front_wheel, time_txt):

    time_txt.set_text('Time: {:1.1f} s'.format(time))
    time_txt.set_position((xR - 3.0, 2.5))

    ax.set_xlim((xR - VIEW_WIDTH / 3, xR + 2 * VIEW_WIDTH / 3))

    rect.set_xy([xR, yRs])
    rect.angle = _np.rad2deg(theta)

    road_R.set_xdata(_np.hstack((xR__hist, xR__futr)))
    road_R.set_ydata(_np.hstack((yR__hist, yR__futr)))

    # TODO : Could plot the road under the front tire only if there is no
    # future road under the rear tire. Need to figure out that logic.
    road_F.set_xdata(_np.hstack((xF__hist, xF__futr)))
    road_F.set_ydata(_np.hstack((yF__hist, yF__futr)))

    rear_suspension.set_xdata([xR, xR])
    rear_suspension.set_ydata([yRu, yRs])

    front_suspension.set_xdata([xF, xF])
    front_suspension.set_ydata([yFu, yFs])

    rear_wheel.center = (xR, yRu)
    front_wheel.center = (xF, yFu)


def rear_suspension_height(yC, theta, a, b):
    """yRs"""
    return yC - a*_np.sin(theta) - b*_np.cos(theta)


def front_suspension_height(yC, theta, a, b, l):
    """yFs"""
    return yC - a*_np.sin(theta) - b*_np.cos(theta) + l*_np.sin(theta)


# Default road, sinusoid
def distance_rear(v, time):
    """xR"""
    return v * time


def distance_front(v, l, theta, time):
    """xF"""
    return (v * time) + l * _np.cos(theta)


def road_height_rear(v, d, Y, time):
    """yR"""
    w = v / d * 2 * _np.pi
    return Y * _np.sin(w * time)


def road_height_front(v, d, Y, xF):
    """yF"""
    w = v / d * 2 * _np.pi
    return Y * _np.sin(w * xF / v)


def road_vertical_velocity_rear(v, d, Y, time):
    """vR"""
    w = v / d * 2 * _np.pi
    return w * Y * _np.cos(w * time)


def road_vertical_velocity_front(v, d, Y, xF):
    """vF"""
    w = v / d * 2 * _np.pi
    return w * Y * _np.cos(w * xF / v)


class HalfCarSystem(MultiDoFNonLinearSystem):

    def __init__(self):

        super(HalfCarSystem, self).__init__()

        mC = 1300  # kg

        self.constants['mR'] = 0.15 * mC / 2  # kg
        self.constants['mF'] = 0.20 * mC / 2  # kg
        self.constants['mC'] = mC  # kg
        self.constants['IC'] = 2100  # kg m**2
        self.constants['kT'] = 135000  # N/m
        self.constants['cT'] = 1400  # kg s
        self.constants['ksR'] = 21000  # N/m
        self.constants['csR'] = 2000  # kg s
        self.constants['ksF'] = 28000  # N/m
        self.constants['csF'] = 2200  # kg s
        self.constants['a'] = 1.7  # m
        self.constants['b'] = 0.6  # m
        self.constants['l'] = 3.0  # m
        self.constants['dw'] = 0.6  # m
        self.constants['lso'] = 0.5  # m
        self.constants['g'] = 9.81  # m/s**2
        self.constants['v'] = 7.5  # m/s
        self.constants['Y'] = 0.01  # m
        self.constants['d'] = 6.0  # m

        self.coordinates['theta'] = 0.0
        self.coordinates['yC'] = 0.0
        self.coordinates['yRu'] = 0.0
        self.coordinates['yFu'] = 0.0

        self.speeds['omega'] = 0
        self.speeds['vC'] = 0
        self.speeds['vRu'] = 0
        self.speeds['vFu'] = 0

        f, states, inputs, constants, t_sym = derive_equations_of_motion()
        udot = first_order_form(f, states, inputs, constants, t_sym)
        eval_udot = lambdify(udot, states, inputs, constants)
        self._eval_equil_eqs = generate_equilibrium_func(f, states, inputs,
                                                         constants, t_sym)

        self.add_measurement('xR', distance_rear)
        self.add_measurement('xF', distance_front)
        self.add_measurement('yR', road_height_rear)
        self.add_measurement('yF', road_height_front)
        self.add_measurement('vR', road_vertical_velocity_rear)
        self.add_measurement('vF', road_vertical_velocity_front)
        self.add_measurement('yRs', rear_suspension_height)
        self.add_measurement('yFs', front_suspension_height)

        self.set_coordinates_to_equilibrium()

        def rhs(theta, yC, yRu, yFu, omega, vC, vRu, vFu,
                mR, mF, mC, IC, kT, cT, ksR, csR, ksF, csF,
                a, b, l, dw, lso, g, xR, xF, yR, yF, vR, vF):

            theta_dot = omega
            yc_dot = vC
            yru_dot = vRu
            yfu_dot = vFu

            omega_dot = eval_udot[0](theta, yC, yRu, yFu, omega, vC, vRu, vFu,
                                     yF, yR, vR, vF,
                                     mR, mF, mC, IC, kT, cT, ksR, csR, ksF, csF,
                                     a, b, l, dw, lso, g)
            vc_dot = eval_udot[1](theta, yC, yRu, yFu, omega, vC, vRu, vFu,
                                  yF, yR, vR, vF,
                                  mR, mF, mC, IC, kT, cT, ksR, csR, ksF, csF,
                                  a, b, l, dw, lso, g)
            vru_dot = eval_udot[2](theta, yC, yRu, yFu, omega, vC, vRu, vFu,
                                   yF, yR, vR, vF,
                                   mR, mF, mC, IC, kT, cT, ksR, csR, ksF, csF,
                                   a, b, l, dw, lso, g)
            vfu_dot = eval_udot[3](theta, yC, yRu, yFu, omega, vC, vRu, vFu,
                                   yF, yR, vR, vF,
                                   mR, mF, mC, IC, kT, cT, ksR, csR, ksF, csF,
                                   a, b, l, dw, lso, g)

            return (theta_dot, yc_dot, yru_dot, yfu_dot, omega_dot, vc_dot,
                    vru_dot, vfu_dot)

        self.diff_eq_func = rhs

        self.config_plot_func = plot_config

        self.config_plot_update_func = plot_update

    def set_coordinates_to_equilibrium(self):
        """When called this function will set the four coordinates to the
        values at equilibrium for the currently specified constants and
        measurements."""

        con_syms = ['mR', 'mF', 'mC', 'IC', 'kT', 'cT', 'ksR', 'csR', 'ksF',
                    'csF', 'a', 'b', 'l', 'dw', 'lso', 'g']
        con_vals = [self.constants[v] for v in con_syms]

        input_vals = [self.measurements['yR'], self.measurements['yF']]

        yRs_guess = (self.measurements['yR'] + self.constants['dw'] / 2 +
                     self.constants['lso'])
        yFs_guess = (self.measurements['yF'] + self.constants['dw'] / 2 +
                     self.constants['lso'])
        theta_guess = _np.arctan2(yFs_guess - yRs_guess, self.constants['l'])
        yC_guess = self.constants['a'] * _np.cos(theta_guess)
        yRu_guess = self.measurements['yR'] + self.constants['dw'] / 2
        yFu_guess = self.measurements['yF'] + self.constants['dw'] / 2

        sol = _fsolve(self._eval_equil_eqs,
                      [theta_guess, yC_guess, yRu_guess, yFu_guess],
                      args=tuple(input_vals + con_vals))

        self.coordinates['theta'] = sol[0]
        self.coordinates['yC'] = sol[1]
        self.coordinates['yRu'] = sol[2]
        self.coordinates['yFu'] = sol[3]


class LinearHalfCarSystem(MultiDoFLinearSystem):

    def __init__(self):

        super(LinearHalfCarSystem, self).__init__()

        mC = 1300  # kg

        self.constants['mR'] = 0.15 * mC / 2  # kg
        self.constants['mF'] = 0.20 * mC / 2  # kg
        self.constants['mC'] = mC  # kg
        self.constants['IC'] = 2100  # kg m**2
        self.constants['kT'] = 135000  # N/m
        self.constants['cT'] = 1400  # kg s
        self.constants['ksR'] = 21000  # N/m
        self.constants['csR'] = 2000  # kg s
        self.constants['ksF'] = 28000  # N/m
        self.constants['csF'] = 2200  # kg s
        self.constants['a'] = 1.7  # m
        self.constants['b'] = 0.6  # m
        self.constants['l'] = 3.0  # m
        self.constants['dw'] = 0.6  # m
        self.constants['lso'] = 0.5  # m
        self.constants['g'] = 9.81  # m/s**2
        self.constants['v'] = 7.5  # m/s
        self.constants['Y'] = 0.01  # m
        self.constants['d'] = 0.5  # m

        f, states, inputs, constants, t_sym = derive_equations_of_motion()
        eval_equil_eqs = generate_equilibrium_func(f, states, inputs,
                                                   constants, t_sym)
        M, C, K, F, eq_syms = linearize_equations_of_motion(f, states, inputs,
                                                            constants, t_sym)
        eval_M, eval_C, eval_K = lambdify_M_C_K(M, C, K, eq_syms, constants)

        self.coordinates['theta'] = 0.0
        self.coordinates['yC'] = 0.0
        self.coordinates['yRu'] = 0.0
        self.coordinates['yFu'] = 0.0

        self.speeds['omega'] = 0
        self.speeds['vC'] = 0
        self.speeds['vRu'] = 0
        self.speeds['vFu'] = 0

        self.add_measurement('xR', distance_rear)
        self.add_measurement('xF', distance_front)
        self.add_measurement('yR', road_height_rear)
        self.add_measurement('yF', road_height_front)
        self.add_measurement('vR', road_vertical_velocity_rear)
        self.add_measurement('vF', road_vertical_velocity_front)
        self.add_measurement('yRs', rear_suspension_height)
        self.add_measurement('yFs', front_suspension_height)

        def can_coeff_matrices(mR, mF, mC, IC, kT, cT, ksR, csR, ksF,
                               csF, a, b, l, dw, lso, g):

            # equilibrium on flat road
            eq_vals = _fsolve(eval_equil_eqs, [0, 0.6, 0.3, 0.3],
                              args=(0.0, 0.0, mR, mF, mC, IC, kT, cT, ksR, csR,
                                    ksF, csF, a, b, l, dw, lso, g))

            M = eval_M(mR, mF, mC, IC, kT, cT, ksR, csR, ksF, csF, a, b, l,
                       dw, lso, g, eq_vals[0], eq_vals[1], eq_vals[2],
                       eq_vals[3])

            C = eval_C(mR, mF, mC, IC, kT, cT, ksR, csR, ksF, csF, a, b, l,
                       dw, lso, g, eq_vals[0], eq_vals[1], eq_vals[2],
                       eq_vals[3])

            K = eval_K(mR, mF, mC, IC, kT, cT, ksR, csR, ksF, csF, a, b, l,
                       dw, lso, g, eq_vals[0], eq_vals[1], eq_vals[2],
                       eq_vals[3])

            return M, C, K

        self.canonical_coeffs_func = can_coeff_matrices
