# Causeway road
def distance_rear(v, d, time):
    return (v * time) % d


def distance_front(v, d, l, theta, time):
    return ((v * time) + l * _np.cos(theta)) % d


def beam_coeffs(d, Y):
    A = _np.array([[d**4, d**3, d],
                   [(d/2)**4, (d/2)**3, d/2],
                   [4*(d/2)**3, 3*(d/2)**2, 1]])
    b = _np.array([0, -Y, 0])
    c1, c2, c3 = _np.linalg.solve(A, b)
    return c1, c2, c3


def road_height_rear(v, d, Y, xR, time):
    c1, c2, c3 = beam_coeffs(d, Y)
    return c1 * xR**4 + c2 * xR**3 + c3 * xR


def road_height_front(v, d, Y, xF):
    c1, c2, c3 = beam_coeffs(d, Y)
    return c1 * xF**4 + c2 * xF**3 + c3 * xF


def road_vertical_velocity_rear(d, Y, v, xR):
    c1, c2, c3 = beam_coeffs(d, Y)
    return (4 * c1 * xR**3 + 3 * c2 * xR**2 + c3) * v


def road_vertical_velocity_front(d, Y, v, xF, l, theta, omega):
    c1, c2, c3 = beam_coeffs(d, Y)
    return ((4 * c1 * xF**4 + 3 * c2 * xF**3 + c3) *
            (-l / v * _np.sin(theta) * omega + v))
