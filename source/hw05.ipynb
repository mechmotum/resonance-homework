{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-c03033d972f4cf00",
     "locked": false,
     "points": 15,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-bf20261696eb3dd7",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-c2b131db30eb893f",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Problem 1: Damping Regimes\n",
    "\n",
    "Your friend's cat likes to sit in the window sill and look outside. Occasionally, however, the cat jumps down from the window onto a bookshelf, causing an annoying rattling noise. Your friend, knowing you're taking a mechanical vibrations class, has hired you to figure out a way to solve this problem.\n",
    "\n",
    "When the cat jumps down onto the bookshelf, it essentially sticks to the shelf and fully imparts its momentum. For a first approximation, you model this system as a simple mass spring damper with a mass equal to the mass of the cat ($3.5~\\mathrm{kg}$) plus the books on the shelf ($10~\\mathrm{kg}$) that is given an initial velocity of $0.5~\\mathrm{m/s}$. The cat demonstrates its trick for you a couple times and you find that the system \"rattles\" (oscillates) about $3$ or $4$ times, twice per second.\n",
    "\n",
    "- Find stiffness and damping constants to suit the observations (i.e. oscillations occur at about $2~\\mathrm{Hz}$ and die out after $2~\\mathrm{s}$) and plot the response to confirm.\n",
    "- Is this system under or overdamped? If you want to stop the noise from occuring, what kind of damping do you want the system to have?\n",
    "- Without modifying the stiffness or damping constants, determine what effect changing the system mass has on the period of the free response and what this implies about the system damping characteristic.\n",
    "- How much mass needs to be added or removed from the system to stop the oscillations altogether?\n",
    "- Is it feasible to add books to or remove books from the shelf to solve the problem?\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we need to import and create a `MassSpringDamperSystem` which will represent the bookshelf + cat system. We can also set up all of the parameters that have been given."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from resonance.linear_systems import MassSpringDamperSystem\n",
    "\n",
    "shelf_sys = MassSpringDamperSystem()\n",
    "\n",
    "m_shelf = 10\n",
    "m_cat = 3.5\n",
    "shelf_sys.constants['mass'] = m_shelf + m_cat\n",
    "\n",
    "shelf_sys.coordinates['position'] = 0\n",
    "shelf_sys.speeds['velocity'] = 0.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now it's time to determine appropriate stiffness and damping values. Through hand iteration, stiffness and damping constants can be found to match the observed oscillatory behavior, verified with a plot of the free response. With the mass set, we can vary the system stiffness to modify the oscillation frequency (higher stiffness leads to faster oscillations), and we can vary the system damping to modify its decay rate (higher damping constant leads to faster decay)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# hand-tuned damping and stiffness to achieve described oscillations\n",
    "shelf_sys.constants['damping'] = 50\n",
    "shelf_sys.constants['stiffness'] = 2000\n",
    "\n",
    "# run the free response\n",
    "traj = shelf_sys.free_response(5)\n",
    "\n",
    "# plot the position in time\n",
    "fig, ax = plt.subplots()\n",
    "ax.set_title('time response')\n",
    "ax.set_xlabel('time (s)')\n",
    "ax.set_ylabel('position (m)')\n",
    "ax.plot(traj.index, traj.position)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The behavior observed initially is **underdamped**. We would like the system to instead be **overdamped** so that no oscillations occur at all."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can create a plot of the system period versus mass by generating an array of mass values (i.e. total mass which is the mass of the cat plus the mass of the shelf) and using the system's `period` function to find the period for each mass value.\n",
    "\n",
    "Choosing the mass values to iterate over in this case is tricky. Through some trial and error, it is found that very small mass values are needed to cause the system period to go to infinity (i.e. transition from underdamped to overdamped)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mass_vals = np.linspace(0.1, 5, 100)\n",
    "per_vals = np.zeros(mass_vals.size)\n",
    "for i, m in enumerate(mass_vals):\n",
    "    shelf_sys.constants['mass'] = m\n",
    "    per_vals[i] = shelf_sys.period()\n",
    "    \n",
    "# plot the period vs. mass\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(mass_vals, per_vals)\n",
    "ax.set_title('critical damping transition')\n",
    "ax.set_xlabel('total mass (kg)')\n",
    "ax.set_ylabel('period (s)')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the plot above, you can see that increasing the mass leads to a higher oscillation period, but this trend does not rapidly asymptote to infinity (it goes to infinity along with infinite mass). This is not the correct direction to achieve overdamping. Instead, the effect of decreasing the mass rapidly reverses and critical damping occurs for a very small mass value (something like 0.3 kg).\n",
    "\n",
    "This means it's not feasible to modify the mass of books on the shelf to solve the problem, because the total mass needed for overdamping is less than the cat itself, i.e. the solution is to get rid of the cat :)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-abb4affc042f57db",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Problem 2: Pedestrian Bridge Lateral Oscillations\n",
    "\n",
    "Pedestrian bridges such as the [Millennium Bridge in London](https://en.wikipedia.org/wiki/Millennium_Bridge,_London) sometimes oscillate laterally because of a positive feedback mechanism where oscillations of the bridge tend to cause pedestrians to sync up their gait, causing the bridge to oscillate more, and so on.\n",
    "\n",
    "You're tasked with designing the damping characteristics of a small bridge, modeled below. The worst case scenario is $20$ people walking together near the center of the bridge, each imparting a synchronized sinusoidal force with an amplitude of $500~\\mathrm{N}$ in some frequency range between about $50$ and $140$ steps per minute (note each step is a half period for lateral forces). The system's mass and stiffness are specified below. Use the frequency response over the specified input frequencies to determine the damping coefficient that will ensure that steady state vibrations stay under $10~\\mathrm{cm}$ for all the anticipated walking frequencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-624f17cd58e003e0",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "outputs": [],
   "source": [
    "from resonance.linear_systems import MassSpringDamperSystem\n",
    "\n",
    "bridge_sys = MassSpringDamperSystem()\n",
    "bridge_sys.constants['mass'] = 10000\n",
    "bridge_sys.constants['stiffness'] = (2*np.pi*0.6)**2 * 10000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-740a248bf464c1a4",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The force amplitude is just the number of people times the force amplitude imparted by each person."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Fo = 20 * 500"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We want to simulate over a range of walking cadences which are given. These rates are given as steps per minute, so we need to convert to radians per second as well as account for the fact that one **period** of the forcing is the time between steps of the *same foot*, so we also divide by 2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "frequencies = 2 * np.pi * np.linspace(50/2, 140/2, num=100) / 60"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can identify the peak displacement caused by this forcing by creating a frequency response plot and taking the peak value (which occurs near but not exactly at the natural frequency). We also want to do this over a range of damping values to identify a damping value that meets our design criterion.\n",
    "\n",
    "To achieve this, we'll iterate over a range of damping values, and for each damping value, we'll iterate over a range of frequencies to get the frequency response. We add the peak of the frequency response to an array and then we can generate a plot of peak response vs. damping value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# iterate over a wide range of damping values\n",
    "c_vals = np.linspace(20000, 50000, 10)\n",
    "max_amps = np.zeros(c_vals.size)\n",
    "for i, c in enumerate(c_vals):\n",
    "    # set the damping constant, then iterate over a range of frequencies\n",
    "    bridge_sys.constants['damping'] = c\n",
    "    amps = []\n",
    "    for omega in frequencies:\n",
    "        traj = bridge_sys.sinusoidal_forcing_response(Fo, omega, 20.0)\n",
    "        # grab the amplitude from the steady portion of the response\n",
    "        amps.append(np.max(traj.position[15:]))\n",
    "    \n",
    "    # add the max for this frequency response to an array\n",
    "    max_amps[i] = np.max(amps)\n",
    "\n",
    "# plot the peak response vs. damping constant\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(c_vals, max_amps)\n",
    "ax.set_xlabel('damping constant [kg/s]')\n",
    "ax.set_ylabel('peak response [m]')\n",
    "ax.grid();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the plot, you can see resonant response falls below 10 cm at around 28500 kg/s."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-b0478f5ccd63457e",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Problem 3: Square Wave Fourier Coefficients\n",
    "\n",
    "Use SymPy to derive the Fourier coefficients $a_0$, $a_n$, and $b_n$ for the square wave below and display them as functions of $n$.\n",
    "\n",
    "![](square_wave.svg)\n",
    "\n",
    "Use the symbolic expressions for $a_0$, $a_n$, and $b_n$ to write a function that **numerically** (i.e. no SymPy) approximates a square wave given an array of time values and a number of terms $N$, i.e. implement\n",
    "\n",
    "$$\n",
    "\\tilde{f}(t) = \\frac{a_0}{2} + \\sum_{n=1}^N \\left( a_n\\cos n\\omega_T t + b_n \\sin n\\omega_T t\\right)\n",
    "$$\n",
    "\n",
    "Make a plot of your approximation for $N = 10$. Plot a real square wave $f(t)$ on the same plot. You can either write a function to generate a square wave in a piece wise fashion or use [`scipy.signal.square`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.square.html#scipy.signal.square).\n",
    "\n",
    "Write a function that computes the root mean square error (RMSE) between a true square wave $f(t)$ and the approximation $\\tilde{f}(t)$, and use it to find the error for various values of $N$. Make a plot to see how much the error decreases as you use more and more coefficients.\n",
    "\n",
    "Does the RMSE ever reach zero as $N$ approaches $\\infty$? ([*hint*](https://en.wikipedia.org/wiki/Gibbs_phenomenon))\n",
    "\n",
    "Your error plot should look like a series of \"steps\" (noticeable in the range where $N$ is small). Why does this occur?\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll start by using SymPy to find the Fourier series coefficients in terms of $n$. First are some symbols with a few assumptions that will help in simplification."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sympy as sm\n",
    "\n",
    "# time\n",
    "t = sm.symbols('t', real=True, positive=True)\n",
    "# series index -- ensuring it's an integer will help later\n",
    "n = sm.symbols('n', integer=True, positive=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looking at the plot of the square wave, we know the period is $2\\pi$. We can form the function describing this square wave in a piecewise fashion, valid for a single period only (this is all we need to evaluate the coefficients).\n",
    "\n",
    "$$\n",
    "f(t) =\n",
    "\\begin{cases} \n",
    "      1 & t \\leq \\pi \\\\\n",
    "      -1 & t > \\pi \\\\\n",
    "\\end{cases}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# period\n",
    "T = 2 * sm.pi\n",
    "\n",
    "# square wave function\n",
    "f = sm.Piecewise((1, t<=sm.pi),\n",
    "                 (-1, t>sm.pi))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can find the coefficients by integrating\n",
    "\n",
    "$$\n",
    "a_n = \\frac{2}{T} \\int_0^T f(t) \\cos(n\\omega t) dt\n",
    "$$\n",
    "\n",
    "$$\n",
    "b_n = \\frac{2}{T} \\int_0^T f(t) \\sin(n\\omega t) dt\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "an = 2 / T * sm.integrate(f * sm.cos(2 * sm.pi / T * n * t), (t, 0, T))\n",
    "an.simplify()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bn = 2 / T * sm.integrate(f * sm.sin(2 * sm.pi / T * n * t), (t, 0, T))\n",
    "bn.simplify()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can switch over to numerically evaluating the Fourier series using these coefficients and some given values of time. Here's a function that generates the Fourier series approximation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def estimate_square(t, num_terms):\n",
    "    \"\"\"Estimates the square wave at times t using a Fourier series\"\"\"\n",
    "    n = np.arange(1, num_terms+1)[:, np.newaxis]\n",
    "    bn = 2 * (-(-1)**n + 1) / (np.pi * n)\n",
    "    # generate a 2D array where each term in the series is row\n",
    "    # then sum across the rows (down the columns)\n",
    "    F = np.sum(bn * np.sin(t*n), axis=0)\n",
    "    return F"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It turns out that `scipy.signal.square` gives us exactly the square wave in the picture, so we can use that for comparison. Here's a plot of the square wave and the Fourier series approximation for $N=10$ terms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy import signal\n",
    "\n",
    "# some time values (a few periods)\n",
    "t = np.arange(0, 6*np.pi, 0.01)\n",
    "# number of terms\n",
    "N = 10\n",
    "\n",
    "f_square = signal.square(t)\n",
    "f_approx = estimate_square(t, num_terms=N)\n",
    "\n",
    "# plot\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(t, f_square, color='k', linewidth=3, alpha=0.4)\n",
    "ax.plot(t, f_approx, color='k')\n",
    "ax.legend(['true square', 'approximation'])\n",
    "ax.set_xlabel('$t$')\n",
    "ax.set_ylabel('amplitude')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we want to plot the RMSE vs. the number of terms used in the Fourier series approximation. First we'll define a function to compute the RMSE between two signals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def rmse(measurements, predictions):\n",
    "    return np.sqrt(np.mean((predictions - measurements)**2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can iterate over some values of $N$, compute the RMSE between the true square wave and the approximation, and then plot these error values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nvals = np.arange(1, 100)\n",
    "errvals = np.zeros(nvals.size)\n",
    "for i, n in enumerate(nvals):\n",
    "    errvals[i] = rmse(estimate_square(t, num_terms=n), signal.square(t))\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(nvals, errvals)\n",
    "ax.set_xlabel('number of terms $N$')\n",
    "ax.set_ylabel('RMSE')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It turns out that even though Gibbs phenomenon says there will always be an overshoot in the Fourier series approximation caused by the discontinuity in the signal we're approximating, the Fourier series approximation is always guaranteed to converge to zero error as $N \\rightarrow \\infty$. This is because each new component is of a higher and higher frequency, so the area under the overshoot goes to zero as it becomes infinitely narrow in time.\n",
    "\n",
    "There are steps in the RMSE plot because every even value of $b_n$ is 0, so the approximation is the same as for the previous value of $N$, hence the error is exactly the same."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
