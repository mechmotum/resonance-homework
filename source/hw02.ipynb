{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Balancing Book\n",
    "\n",
    "When we experimented with the balancing book system in class, we saw that the period of oscillation depends on the book's geometry because the geometry affects its moment of inertia. Let's now investigate the influence of the book's mass on the oscillation.\n",
    "\n",
    "We'll start with our usual imports."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll need a function to estimate the period of oscillation from a simulated free response. Implement a function below that estimates the period of a sinusoid given an array of time values and an array of the sinusoid's values, in that order. Use the function below to generate a sinusoid with a period of 2 seconds to test your function, i.e. generate a sinusoid using `generate_sinusoid`, then plug it's return values into your period estimation function to check that the period it returns is 2 seconds. Take note of the arguments the `generate_sinusoid` function takes and returns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def generate_sinusoid(freq):\n",
    "    \"\"\"Generate a sinusoid of a given frequency.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    freq : float\n",
    "        Sinusoid frequency, in Hz.\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    t : ndarray, shape (n,)\n",
    "        An array of time values at which the sinusoid is sampled.\n",
    "    x : ndarray, shape (n,)\n",
    "        The values of the sinusoid.\n",
    "    \"\"\"\n",
    "    t = np.arange(0, 5, 1/(10*freq))\n",
    "    x = np.sin(2*np.pi*freq*t)\n",
    "    return t, x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-5b720ce53dcd39c0",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "def estimate_period(time, signal):\n",
    "    peak_idxs = np.diff(np.sign(signal)) < 0\n",
    "    peak_idxs = np.hstack((peak_idxs, False))\n",
    "    period = np.diff(time[peak_idxs]).mean()\n",
    "    return period\n",
    "    \n",
    "t, x = generate_sinusoid(0.5)\n",
    "estimate_period(t, x)\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's get the `BookOnCupSystem` so we can work with it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from resonance.linear_systems import BookOnCupSystem\n",
    "sys = BookOnCupSystem()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recall that the system has the following constant parameters. We'll leave them as defaults for now."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.constants"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also, this system has a single coordinate, the book angle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.coordinates"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our goal now is to make a plot of the system's free response period for various book mass values. To achieve this, we'll make use of a `for` loop. The idea is that for each mass value, we set the system's mass, generate the free response, compute the period, and then add it to an array of period values so we can plot them. The `for` loop just lets us specify the instructions to do this once, then repeat the action for several mass values.\n",
    "\n",
    "A Python for loop typically looks something like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for val in np.arange(0, 5):\n",
    "    print(val)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This recipe iterates over the array generated by `np.arange(0, 5)`, sets the current value to `val`, and allows us to use `val` inside the *body* of the loop.\n",
    "\n",
    "If the array already exists, we can just use the variable like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vals = np.arange(0, 5)\n",
    "for val in vals:\n",
    "    print(val)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One more recipe that can be useful is to allocate another array that will store some values and fill them in inside the loop. The `enumerate` function gives us both an index and the current value in an array as we loop over it. Here's what this recipe looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# array of sinusoid periods, for example\n",
    "periods = np.linspace(1, 2, 5)\n",
    "# \"empty\" array the same shape as periods array\n",
    "frequencies = np.zeros_like(periods)\n",
    "\n",
    "# iterate over each period value and fill in the elements of\n",
    "# the frequencies array with the corresponding frequency (in Hz)\n",
    "for index, period in enumerate(periods):\n",
    "    frequencies[index] = 1 / period\n",
    "    \n",
    "frequencies"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we're set up to use this last recipe to compute the balancing book's period for various mass values. Below, an array of mass values is given as well as an empty array that will hold the period values. Set the initial angle of the system to **5 degrees**, then for each mass value, use your period estimation function to find the period of the free response.\n",
    "\n",
    "*Note: if you were not able to get a period estimation function working, you an use the `estimate_period` function obtained with `from resonance.functions import estimate_period`.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-b2ac15cb82672f69",
     "locked": false,
     "points": 2,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "masses = np.linspace(0.1, 1.5, 10)\n",
    "periods = np.zeros_like(masses)\n",
    "\n",
    "### BEGIN SOLUTION\n",
    "sys.coordinates['book_angle'] = np.deg2rad(5)\n",
    "for idx, mass in enumerate(masses):\n",
    "    sys.constants['mass'] = mass\n",
    "    trajectories = sys.free_response(5.0)\n",
    "    period = estimate_period(trajectories.index, trajectories.book_angle)\n",
    "    periods[idx] = period\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now make a plot showing the period versus mass for each of the pairs generated above. Use circles to indicate the points plotted rather than a line. Be sure to label your axes appropriately. Does the period depend on the mass of the book?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-c981db1562961673",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "fig, ax = plt.subplots(1, 1)\n",
    "ax.plot(masses, periods, 'o')\n",
    "ax.set_xlabel('mass [kg]')\n",
    "ax.set_ylabel('period [s]')\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-5e6f6de547377986",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When the book system is displaced from its equilibrium, its center of mass is raised vertically, so gravity acts to bring the center of mass back downward once the book is released from this initial angle. Since the book has inertia, it doesn't just stop suddenly once the equilibrium angle is achieved--instead, momentum carries it past the equilibrium angle and this process repeats to create the oscillations you observe.\n",
    "\n",
    "Aside from the center of mass, there is another point of interest that determines whether or not the book will simply roll off the side of the cup, and that is the contact point between the book and the cup.\n",
    "\n",
    "Add the following measurements to the system:\n",
    "\n",
    "- The horizontal (x) position of the contact point between the book and the cup, measured from the center of the cup.\n",
    "- The horizontal (x) position of the center of mass of the book, also measured from the center of the cup.\n",
    "\n",
    "Once you have added the measurement functions, set the system thickness to **8 centimeters**. Then set the initial *velocity* to **30 degrees per second** (*hint: `sys.speeds['book_angle_vel'] = ...`*). Finally, simulate the free response for **10 seconds** and plot plot $x_{\\textrm{cp}} - x_{\\textrm{com}}$, the difference between the contact point x coordinate and the center of mass x coordinate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-c102bc982ae3e49f",
     "locked": false,
     "points": 6,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "def center_x(radius, book_angle, length, thickness):\n",
    "    x = radius * np.sin(book_angle) - \\\n",
    "        radius * book_angle * np.cos(book_angle) + \\\n",
    "        thickness / 2.0 * np.sin(book_angle)\n",
    "    return x\n",
    "        \n",
    "def contact_x(radius, book_angle):\n",
    "    x = radius * np.sin(book_angle)\n",
    "    return x\n",
    "    \n",
    "sys = BookOnCupSystem()\n",
    "sys.constants['thickness'] = 0.08\n",
    "sys.speeds['book_angle_vel'] = np.deg2rad(30)\n",
    "sys.add_measurement('center_x', center_x)\n",
    "sys.add_measurement('contact_x', contact_x)\n",
    "trajectories = sys.free_response(10.0)\n",
    "diff = trajectories.contact_x - trajectories.center_x\n",
    "plt.plot(trajectories.index, diff)\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trajectories.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In your plot, you should see that initially that the difference is zero. Since we didn't give the book an initial angle, the center of mass is directly over top of the contact point. As the book rotates, however, there is an interesting interaction between these two points. Why does this difference seem to oscillate at a different rate than the book angle? When the book is at its maximum angle, why isn't the difference at its maximum? What would it mean for the difference to go all the way back to zero at the maximum book angle? An image may be useful in helping describe these conditions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-9ff351e3c60e722d",
     "locked": false,
     "points": 2,
     "schema_version": 1,
     "solution": true
    }
   },
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Torsional Pendulum\n",
    "\n",
    "In class, we saw the relationship between the vibration period $T$ and the moment of inertia $I$ of a bicycle wheel mounted to a torsional spring follows a square root law:\n",
    "\n",
    "$$T \\propto \\sqrt{I}$$\n",
    "\n",
    "which ends up becoming\n",
    "\n",
    "$$T = 2\\pi\\sqrt{\\frac{I}{k}}$$\n",
    "\n",
    "where $T$ is the oscillation period, $I$ is the moment of inertia, and $k$ is the stiffness of the torsional spring.\n",
    "\n",
    "If we know the properties of the torsional spring, we can use the equation above to compute the unknown moment of inertia of an object once we measure the period of oscillation. If, however, we aren't sure what our torsional spring is made out of, describe how you would go about determining the moment of inertia of your object using a second object with well-known inertial parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-4ccc5e73ed52f007",
     "locked": false,
     "points": 2,
     "schema_version": 1,
     "solution": true
    }
   },
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Test your process by creating two different `TorsionalPendulumSystem` systems. Set their `stiffness` to the same value (that you pretend not to know), but give them different `rotational_inertia` constants. Show that you are able to estimate the rotational inertia of the \"unknown\" system. You may use multiple cells for your answer, the cell below will just be for grading and feedback."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-7f296e59a9b02357",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "from resonance.linear_systems import TorsionalPendulumSystem\n",
    "\n",
    "# pretend we don't know these\n",
    "stiffness = 10.0\n",
    "unknown_inertia = 0.2\n",
    "\n",
    "known_sys = TorsionalPendulumSystem()\n",
    "known_sys.constants['torsional_stiffness'] = stiffness\n",
    "known_sys.constants['rotational_inertia'] = 0.1\n",
    "\n",
    "unknown_sys = TorsionalPendulumSystem()\n",
    "unknown_sys.constants['torsional_stiffness'] = stiffness\n",
    "unknown_sys.constants['rotational_inertia'] = unknown_inertia\n",
    "\n",
    "known_sys.coordinates['torsion_angle'] = np.deg2rad(3)\n",
    "resp = known_sys.free_response(5)\n",
    "T_known = estimate_period(resp.index, resp.torsion_angle)\n",
    "\n",
    "unknown_sys.coordinates['torsion_angle'] = np.deg2rad(3)\n",
    "resp = unknown_sys.free_response(5)\n",
    "T_unknown = estimate_period(resp.index, resp.torsion_angle)\n",
    "\n",
    "k = (2 * np.pi * np.sqrt(known_sys.constants['rotational_inertia']) / T_known)**2\n",
    "I_unknown = (T_unknown / (2*np.pi))**2 * k\n",
    "print(\"actual inertia: {}\".format(unknown_inertia))\n",
    "print(\"experimentally determined inertia: {}\".format(I_unknown))\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When performing this experiment with a physical apparatus, how carefully do you need to be when setting the initial angle for the two measurements (one for the \"known\" system and one for the \"unknown\" system)? In other words, do you need to be consistent in setting the initial angle for both measurements? Why or why not?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-6bdb61d1b0d53783",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes the initial state of a system involves an initial displacement *and* an initial speed. Below, a torsional pendulum has been set up with an initial angle of 2 degrees and an initial angular velocity of 10 degrees per second. The torsion angle's free response is then plotted."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys = TorsionalPendulumSystem()\n",
    "sys.constants['torsional_stiffness'] = 12.0\n",
    "sys.constants['rotational_inertia'] = 0.3\n",
    "\n",
    "# initial angle of 2 degrees\n",
    "sys.coordinates['torsion_angle'] = np.deg2rad(2)\n",
    "# initial angular velocity of 3 degrees per second\n",
    "sys.speeds['torsion_angle_vel'] = np.deg2rad(10)\n",
    "\n",
    "trajectories = sys.free_response(5.0)\n",
    "trajectories.torsion_angle.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This now looks like a sinusoid with not only an amplitude and a frequency, but a phase as well:\n",
    "\n",
    "$$\\theta(t) = A \\cos\\left(\\omega t + \\phi\\right)$$\n",
    "\n",
    "Use the `curve_fit` function to fit a phase-shifted cosine function to the torsion angle response above and show the fitted parameters. Remember that initial guesses for the parameters may be important."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-fd0c065a0e88c804",
     "locked": false,
     "points": 3,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "from scipy.optimize import curve_fit\n",
    "\n",
    "### BEGIN SOLUTION\n",
    "def cos_func(times, amp, freq, phase):\n",
    "    return amp * np.cos(freq * times + phase)\n",
    "\n",
    "popt, pcov = curve_fit(cos_func,\n",
    "                       trajectories.index,\n",
    "                       trajectories.torsion_angle,\n",
    "                       p0=(0.4, 2*np.pi, 0))\n",
    "popt\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What phase angle (in degrees) did the curve fit come up with? Does this make sense looking at the plot?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-3b5da7f1c69a9ca5",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
