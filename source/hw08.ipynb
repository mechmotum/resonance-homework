{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-7ce9d049a8bba963",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-30df4006876f4b86",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Problem 1\n",
    "\n",
    "Consider the system below consisting of two pendulums coupled by a spring. Determine the natural frequencies and mode shapes. Plot the mode shapes (representing them however you like). Then plot the free response of the system to an initial condition consisting of the *first mode shape* for $k = 20~\\textrm{N/m}$, $l = 0.5~\\textrm{m}$, and $m_1 = m_2 = 10~\\textrm{kg}$, $a = 0.1~\\textrm{m}$ along the pendulum.\n",
    "\n",
    "![](coupled-springs.svg)\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "problem-01",
     "locked": false,
     "points": 10,
     "schema_version": 1,
     "solution": true
    }
   },
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll derive the equations of motion by summing moments about the pivot point of each pendulum. The first pendulum has a gravitational force acting downward and, if the spring is in stretch (the attachment point of the second pendulum has moved farther to the right than the attachment point of the first pendulum), the spring acting horizontally along with its motion.\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "m l^2 \\ddot{\\theta}_1 &= ka \\left(a\\sin\\theta_2 - a\\sin\\theta_1\\right)\\cos\\theta_1 - mgl\\sin\\theta_1\\\\\n",
    "                      &= ka^2 \\left(\\theta_2 - \\theta_1 \\right) - mgl\\theta_1\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "The second pendulum also has a gravitational force and the same spring force but acting against its motion\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "m l^2 \\ddot{\\theta}_2 &= -ka \\left(a\\sin\\theta_2 - a\\sin\\theta_1\\right)\\cos\\theta_2 - mgl\\sin\\theta_2\\\\\n",
    "                      &= ka^2 \\left(\\theta_1 - \\theta_2 \\right) - mgl\\theta_2\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Linearizing ($\\sin\\theta \\approx \\theta$, $\\cos\\theta \\approx 1$) and then converting to matrix form gives:\n",
    "\n",
    "$$\n",
    "\\begin{bmatrix}\n",
    "    ml^2 & 0 \\\\\n",
    "    0 & ml^2\n",
    "\\end{bmatrix}\n",
    "\\begin{bmatrix}\n",
    "    \\ddot{\\theta}_1 \\\\ \\ddot{\\theta}_2\n",
    "\\end{bmatrix}\n",
    "+\n",
    "\\begin{bmatrix}\n",
    "    ka^2 + mgl & -ka^2 \\\\ -ka^2 & ka^2 + mgl\n",
    "\\end{bmatrix}\n",
    "\\begin{bmatrix}\n",
    "    \\theta_1 \\\\ \\theta_2\n",
    "\\end{bmatrix}\n",
    "=\n",
    "0\n",
    "$$\n",
    "\n",
    "To find the natural frequencies and mode shapes of the system, we'll need to take the coefficient matrices above and solve the eigenvalue problem. First, we'll set up system constants according to the problem statement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = 10   # pendulum mass (same for both) [kg]\n",
    "k = 20   # coupling spring stiffness [N/m]\n",
    "l = 0.5  # pendulum length (same for both) [m]\n",
    "a = 0.1  # attachment point along the pendulum [m]\n",
    "g = 9.81 # gravitational constant [N/kg]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can plug in the expressions above to evaluate $M$ and $K$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# mass matrix\n",
    "M = m * l**2 * np.eye(2)\n",
    "\n",
    "# stiffness matrix\n",
    "K = np.array([[k*a**2 + m*g*l, -k*a**2],\n",
    "              [-k*a**2, k*a**2 + m*g*l]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get the mass normalized stiffness, we can compute $M^{-1/2}KM^{-1/2}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# mass-normalized stiffness matrix\n",
    "M_12 = np.linalg.cholesky(M)\n",
    "M_m12 = np.linalg.inv(M_12)\n",
    "K_tilde = M_m12 @ K @ M_m12"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's confirm that it's symmetric:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "K_tilde"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need to find the eigenvalues and eigenvectors of $\\tilde{K}$ to get the natural frequencies as well as the mode shapes (in the intermediate coordinate system)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# eigenvalues and eigenvectors\n",
    "eigvals, eigvecs = np.linalg.eig(K_tilde)\n",
    "\n",
    "natural_freqs = np.sqrt(eigvals)\n",
    "natural_freqs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The natural frequencies then are\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\omega_1 &= 4.447\\,\\textrm{rad/s} \\\\\n",
    "\\omega_2 &= 4.429\\,\\textrm{rad/s}\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "The `eigvecs` array above is the same as the $P$ matrix, so we can multiply it by $M^{-1/2}$ to get the matrix of mode shapes $S$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "S = M_m12 @ eigvecs\n",
    "S"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then the mode shapes are\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\textbf{u}_1 &= \\begin{bmatrix} 0.4472\\\\-0.4472\\end{bmatrix}\\\\\n",
    "\\textbf{u}_2 &= \\begin{bmatrix} 0.4472\\\\0.4472\\end{bmatrix}\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "To plot the mode shapes, we'll just plot arrows to represent the motion of the first and second pendulums."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(nrows=2)\n",
    "for i, (ax, u) in enumerate(zip(axes, S.T)):\n",
    "    ax.quiver([0, 0], [-0.1, 0.1], u, [0, 0])\n",
    "    ax.set_title('mode shape {}'.format(i))\n",
    "    ax.set_ylim(-0.2, 0.2)\n",
    "    ax.set_xlim(-0.5, 0.5)\n",
    "fig.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's clear from this plot that the two modes are:\n",
    "\n",
    "- the pendulums are out of phase and swing against one another\n",
    "- the pendulums are in phase and swing together\n",
    "\n",
    "We were told that the inital condition is the first mode shape (zero initial velocity). We'll first write a function that computes the system trajectory in modal coordinates, which requires an array of time values, the natural frequencies (as a column vector), and the initial position and velocity as column vectors. The function is capable of returning all modal trajectories, where each modal trajectory is a row in the returned array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def rt(t, w, r0, rd0):\n",
    "    \"\"\"Calculate the modal trajectories\"\"\"\n",
    "    return np.sqrt(w**2*r0**2 + rd0**2) / w * \\\n",
    "               np.sin(w*t + np.arctan2(w*r0, rd0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll use this function to generate the modal trajectories, then convert to the original coordinate system via $S$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# inverse of the matrix of mode shapes\n",
    "S_m1 = eigvecs.T @ M_12\n",
    "\n",
    "# initial condition is first mode shape, transformed to modal coords\n",
    "# make sure shape is (2, 1)\n",
    "r0 = (S_m1 @ S[:, 0])[:, None]\n",
    "\n",
    "# initial velocities are zero\n",
    "rd0 = np.zeros((2, 1))\n",
    "\n",
    "# array of time values\n",
    "t = np.arange(0, 5, 0.01)\n",
    "\n",
    "r_vals = rt(t, natural_freqs[:, None], r0, rd0)\n",
    "theta_vals = S @ r_vals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can plot the response."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "for i in range(len(theta_vals)):\n",
    "    ax.plot(t, theta_vals[i], label=r'$\\theta_{}$'.format(i+1))\n",
    "    \n",
    "ax.legend()\n",
    "ax.set_ylabel(r'$\\theta(t)$')\n",
    "ax.set_xlabel(r'$t$')"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
