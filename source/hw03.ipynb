{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bicycle Wheel Animation\n",
    "\n",
    "Create an animation of the apparatus used to estimate the moment of inertia of a bike wheel about its axle. For this problem, we'll walk through the process for the most part, with some pieces left out for you to fill in. We'll start with the usual imports."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's the system we want to animate. This system has the same constants as the rig set up to measure the axial moment of inertia of a bike wheel from the in-class notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from resonance.linear_systems import CompoundPendulumSystem\n",
    "sys = CompoundPendulumSystem()\n",
    "\n",
    "g = 9.81\n",
    "mass = 1.55\n",
    "radius = 0.336\n",
    "dist_to_pivot = 0.296\n",
    "inertia_about_axle = mass * radius**2\n",
    "inertia_about_joint = inertia_about_axle + mass*dist_to_pivot**2\n",
    "\n",
    "sys.constants['acc_due_to_gravity'] = g\n",
    "sys.constants['inertia_about_joint'] = inertia_about_joint\n",
    "sys.constants['joint_to_mass_center'] = dist_to_pivot\n",
    "sys.constants['pendulum_mass'] = mass\n",
    "sys.constants['radius'] = radius"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to draw circles representing the bike tire and the rod the wheel pivots on, we need to know the location of their centers (circles in matplotlib are defined by the center and the radius). We'll just say the pivot point is at $(0, 0)$, and it stays in place, so we'll just need measurements of the wheel center, then.\n",
    "\n",
    "Below, create two functions, called `axle_x` and `axle_y` which take any system constants necessary and return the $x$ coordinate and $y$ coordinate of the bike wheel's center, respectively. Add these measurements to the system defined above (use the same names: `axle_x` and `axle_y`), then simulate the free response to an initial angle of 5 degrees for 4 seconds and plot the trajectories. Make sure `axle_x` and `axle_y` are plotted."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-3cb490155e5ba4f4",
     "locked": false,
     "points": 2,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "def axle_x(joint_to_mass_center, angle):\n",
    "    return joint_to_mass_center * np.sin(angle)\n",
    "\n",
    "def axle_y(joint_to_mass_center, angle):\n",
    "    return -joint_to_mass_center * np.cos(angle)\n",
    "\n",
    "sys.add_measurement('axle_x', axle_x)\n",
    "sys.add_measurement('axle_y', axle_y)\n",
    "\n",
    "sys.coordinates['angle'] = np.deg2rad(5)\n",
    "traj = sys.free_response(4.0)\n",
    "traj.plot(subplots=True);\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's work on the configuration plot. Fill in the `create_plot` function below. Make a transparent circle with a thick black outline to represent the bike wheel, and call it `wheel`. Make a small grey filled-in circle to represent the axle, and call it `axle`. Finally, make a small green filled-in circle to represent the pivot point, and call it `pivot`. We won't worry about drawing spokes.\n",
    "\n",
    "Notice the circles have been added to the plot for you, so you just need to work on making them look correct. You can just run the cell below to see the system in a static configuration and check its appearance. Have a look at matplotlib's documentation for `Circle` to see how to modify the appearance: https://matplotlib.org/devdocs/api/_as_gen/matplotlib.patches.Circle.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-edf0afc2074f3cf3",
     "locked": false,
     "points": 3,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "from matplotlib.patches import Circle\n",
    "\n",
    "def create_plot(radius, axle_x, axle_y):\n",
    "    fig, ax = plt.subplots(1, 1)\n",
    "    ax.set_xlim(-1.5*radius, 1.5*radius)\n",
    "    ax.set_ylim(-2.5*radius, 0.5*radius)\n",
    "    ax.set_aspect('equal')\n",
    "    \n",
    "    ### BEGIN SOLUTION\n",
    "    wheel = Circle((axle_x, axle_y), radius=radius,\n",
    "                   facecolor='none', linewidth=10,\n",
    "                   linestyle='-', edgecolor='k')\n",
    "    pivot = Circle((0.0, 0.0), radius=radius/20,\n",
    "                   color='g')\n",
    "    axle = Circle((axle_x, axle_y), radius=radius/10,\n",
    "                  color='#777777')\n",
    "    ### END SOLUTION\n",
    "    \n",
    "    ax.add_patch(wheel)\n",
    "    ax.add_patch(pivot)\n",
    "    ax.add_patch(axle)\n",
    "    \n",
    "    return fig, wheel, axle\n",
    "\n",
    "sys.config_plot_func = create_plot\n",
    "sys.plot_configuration();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fill in the following config plot update function so we can animate the wheel's motion. Note that to change a `Circle` position, you need the following syntax:\n",
    "\n",
    "```python\n",
    "circle.center = x_coordinate, y_coordinate\n",
    "```\n",
    "\n",
    "(i.e. adapt the line above to update the circles representing the wheel and axle)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-6e9356594017e3b1",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "def update_plot(axle_x, axle_y, wheel, axle):\n",
    "    ### BEGIN SOLUTION\n",
    "    wheel.center = axle_x, axle_y\n",
    "    axle.center = axle_x, axle_y\n",
    "    ### END SOLUTION\n",
    "    \n",
    "sys.config_plot_update_func = update_plot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can run the cell below to watch the system oscillate (note this may take some time to run--it is rendering a video and embedding it in the notebook)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import HTML\n",
    "HTML(sys.animate_configuration(\n",
    "    interval=1000*(sys.result.index[1]-sys.result.index[0]),\n",
    "    save_count=len(sys.result)).to_html5_video())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Center of Percussion\n",
    "\n",
    "In class, we saw that a compound pendulum has what's called a center of percussion, which is at a point along the pendulum corresponding to the length of a simple pendulum that has the same period. We also experimented with an interactive plot to manually determine the length of a simple pendulum to match the free response of the two types of pendulum.\n",
    "\n",
    "Let the angle of the simple pendulum in time be represented by\n",
    "\n",
    "$$\n",
    "\\theta(t) = \\theta_0 \\cos\\left(\\sqrt{\\frac{g}{l}} t\\right)\n",
    "$$\n",
    "\n",
    "where $\\theta_0$ is the initial angle that the pendulum is released from, $g$ is the gravitational constant, and $l$ is the length of the pendulum. We'll see later in the class how this is derived.\n",
    "\n",
    "Use SciPy's `curve_fit` function to determine the length of a simple pendulum which matches the period of the compound pendulum system defined below. You will want to simulate the free response of the compound pendulum, fit the function above to the response, then show the value of $l$ that `curve_fit` finds. Note you may need to play around with the initial guess for the curve fit operation.\n",
    "\n",
    "Demonstrate the goodness-of-fit by setting up a `SimplePendulumSystem` with the length found by `curve_fit` and plotting its response to an initial angle of $\\theta_0$ along with the compound pendulum system's free response to the same initial angle. Plot one of them with a dashed line so you can tell that there are two plots which overlap."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from resonance.linear_systems import CompoundPendulumSystem, SimplePendulumSystem\n",
    "\n",
    "# initial angle\n",
    "theta0 = np.deg2rad(10)\n",
    "# gravitational constant\n",
    "g = 9.81\n",
    "\n",
    "cpend_sys = CompoundPendulumSystem()\n",
    "cpend_sys.constants['acc_due_to_gravity'] = g\n",
    "cpend_sys.constants['inertia_about_joint'] = 0.3\n",
    "cpend_sys.constants['joint_to_mass_center'] = 0.2\n",
    "cpend_sys.constants['pendulum_mass'] = 1.6"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-aca713bfc46256f7",
     "locked": false,
     "points": 8,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "from scipy.optimize import curve_fit\n",
    "\n",
    "cpend_sys.coordinates['angle'] = theta0\n",
    "cpend_traj = cpend_sys.free_response(5.0)\n",
    "\n",
    "def spend_func(times, l):\n",
    "    return theta0 * np.cos(np.sqrt(g/l) * times)\n",
    "\n",
    "popt, pcov = curve_fit(spend_func,\n",
    "                       cpend_traj.index,\n",
    "                       cpend_traj.angle,\n",
    "                       p0=(0.8,))\n",
    "\n",
    "\n",
    "print(\"length: {:.3f}\".format(popt[0]))\n",
    "\n",
    "spend_sys = SimplePendulumSystem()\n",
    "spend_sys.constants['acc_due_to_gravity'] = g\n",
    "spend_sys.constants['pendulum_mass'] = 1.55\n",
    "spend_sys.constants['pendulum_length'] = popt[0]\n",
    "\n",
    "spend_sys.coordinates['angle'] = theta0\n",
    "spend_traj = spend_sys.free_response(5.0)\n",
    "\n",
    "fig, ax = plt.subplots(1, 1)\n",
    "ax.plot(cpend_traj.index, cpend_traj.angle)\n",
    "ax.plot(spend_traj.index, spend_traj.angle, '--')\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using Experimental Data\n",
    "\n",
    "Suppose you are given the following plot (you can ignore the code that generates it). This data comes directly from an accelerometer placed on a system which you've modeled as having a frequency\n",
    "\n",
    "$$\n",
    "\\omega = \\sqrt{\\frac{k}{m}}\n",
    "$$\n",
    "\n",
    "where $\\omega$ is the frequency in **radians per second**, $k$ is the system stiffness (N/m), and $m$ is the system mass (kg). We will see where this equation comes from later on in the course. Recall that the period (in seconds) of a signal is the reciprocal of the frequency (in Hz), and that a frequency in Hz can be converted to rad/s by multiplying by $2\\pi$. Given that the system mass is known to be 10 kg, approximate the system stiffness.\n",
    "\n",
    "Note: Do not use the `estimate_period` function--the data is too noisy for simple peak or zero crossing detection to work properly. (Hint: do it just by visually inspecting the plot).\n",
    "\n",
    "Also note: perform the calculations using the blank code cell provided. Make sure someone else can follow your work--don't just perform the calculation in your head and write down the answer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this code just generates the plot for you to inspect visually--you can ignore it\n",
    "np.random.seed(42)\n",
    "t = np.arange(0, 5, 0.01)\n",
    "x = 0.3 * np.cos(2*np.pi*(np.random.rand()+2)*t + np.pi/6) * \\\n",
    "    np.exp(-0.3*t) + \\\n",
    "    0.03*np.random.randn(*t.shape)\n",
    "fig, ax = plt.subplots(1, 1)\n",
    "ax.plot(t, x)\n",
    "ax.set_xlabel('time [s]')\n",
    "ax.set_ylabel('acceleration [m/s^2]')\n",
    "ax.grid()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-24db8d309c1e02d4",
     "locked": false,
     "points": 2,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "t_first_peak = 0.355\n",
    "t_last_peak = 4.585\n",
    "num_periods = 10\n",
    "period = (t_last_peak - t_first_peak) / num_periods\n",
    "w = 2 * np.pi / period\n",
    "m = 10\n",
    "k = w**2 * m\n",
    "print('system stiffness: {:.2f} N/m'.format(k))\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Series and Parallel Springs\n",
    "\n",
    "Read about the stiffness of springs when they are connected in series and in parallel here: https://en.wikipedia.org/wiki/Series_and_parallel_springs\n",
    "\n",
    "Come up with two stiffness values $k_1$ and $k_2$, representing two different coil springs you have. Use the `MassSpringDamperSystem` to determine the period of oscillation when you connect them individually, in series, and in parallel (i.e. set the `stiffness` of the `MassSpringDamperSystem` to the equivalent stiffness for each spring configuration)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-3371b653c9060239",
     "locked": false,
     "points": 3,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "from resonance.linear_systems import MassSpringDamperSystem\n",
    "from resonance.functions import estimate_period\n",
    "\n",
    "sys = MassSpringDamperSystem()\n",
    "\n",
    "k1 = 100\n",
    "k2 = 250\n",
    "\n",
    "k_parallel = k1 + k2\n",
    "k_series = 1/(1/k1 + 1/k2)\n",
    "\n",
    "for k, cond in zip([k1, k2, k_parallel, k_series],\n",
    "                   ['k1', 'k2', 'par', 'ser']):\n",
    "    sys.coordinates['position'] = 0.05\n",
    "    sys.constants['stiffness'] = k\n",
    "    traj = sys.free_response(k)\n",
    "    period = estimate_period(traj.index, traj.position)\n",
    "    print('{}: k={:.2f} N/m, T={:.2f} s'.format(cond, k, period))\n",
    "    \n",
    "print('parallel increases stiffness, decreases period')\n",
    "print('series decreases stiffness, increases period')\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What do you observe about the effect of the different spring configurations on the equivalent stiffness and the period?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-59e19ad70bf4581f",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tennis Racket\n",
    "\n",
    "Using a `CompoundPendulumSystem`, model a simple tennis racket that hangs from a frictionless pivot and swings as though it's hitting a ball (i.e. the plane formed by the racket's strings is perpendicular to the swinging direction).\n",
    "\n",
    "![](tennis_racket_pendulum.svg)\n",
    "\n",
    "Use a circular ring to represent the head of the racket (ignore the \"strings\") and a cylindrical rod to represent the handle. Use whatever parameters you think are reasonable.\n",
    "\n",
    "Use your system to find the period, and then determine the center of percussion of the racket assuming the period of a *simple pendulum* is:\n",
    "\n",
    "$$\n",
    "T = 2 \\pi \\sqrt{\\frac{l}{g}}\n",
    "$$\n",
    "\n",
    "Discuss where the center of percussion is in relation to the center of the racket head.\n",
    "\n",
    "A few notes:\n",
    "\n",
    "- This is a somewhat open-ended problem to give you a flavor of working out a nontrivial task in a notebook. As such, please use multiple cells to answer this question (the cell below is optional and it will just be where I enter your score and provide written feedback).\n",
    "- Split up code into cells based on small units of work with a Markdown cell between code cells to explain your thought process for each one.\n",
    "- It's not particularly important how accurately your system models a tennis racket (i.e. don't spend too much time worrying about what the mass, lengths, etc. should be). What's important is that you can use the tools we've been working with and you can demonstrate your process for solving the problem.\n",
    "\n",
    "In this system, we're analyzing the oscillation of the entire racket swinging around, but tennis rackets also vibrate when they impact a ball. You can read about this kind of vibration and the center of percussion here: http://www.physics.usyd.edu.au/~cross/tennis.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-c018184e8f44a423",
     "locked": false,
     "points": 10,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "racket = CompoundPendulumSystem()\n",
    "\n",
    "# settings\n",
    "g = 9.81  # gravitational constant\n",
    "m_handle = 0.2  # handle mass, kg\n",
    "l_handle = 0.3  # handle length, m\n",
    "m_head = 0.3  # head mass, kg\n",
    "r_head = 0.15  # head radius, m\n",
    "\n",
    "# computing some inertias\n",
    "I_handle_pivot = m_handle * l_handle**2 / 3\n",
    "I_head_center = m_head * r_head**2 / 2\n",
    "I_head_pivot = m_head * (l_handle + r_head)**2 + I_head_center\n",
    "I_total = I_handle_pivot + I_head_pivot\n",
    "\n",
    "# center of mass\n",
    "m_total = m_handle + m_head\n",
    "com = (m_handle*(l_handle/2) + m_head*(l_handle+r_head))/m_total\n",
    "\n",
    "racket.constants['acc_due_to_gravity'] = g\n",
    "racket.constants['joint_to_mass_center'] = com\n",
    "racket.constants['inertia_about_joint'] = I_total\n",
    "racket.constants['pendulum_mass'] = m_total\n",
    "\n",
    "period = racket.period()\n",
    "cop = (period / (2*np.pi))**2 * g\n",
    "\n",
    "print(\"center of mass: {:.3f}\".format(racket.constants['joint_to_mass_center']))\n",
    "print(\"center of head: {:.3f}\".format(l_handle + r_head))\n",
    "print(\"center of percussion: {:.3f}\".format(cop))\n",
    "print(\"center of percussion is just a bit 'up the handle' from the head center\")\n",
    "### END SOLUTION"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
