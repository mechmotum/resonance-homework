{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ENG 122 Fall 2017 Final Project\n",
    "\n",
    "## Due: 6 PM Tuesday, December 12th\n",
    "\n",
    "Rules:\n",
    "\n",
    "- You may use any reference materials that you want: notes, books, internet, etc.\n",
    "- **No discussion or collaboration whatsoever in any form with any other person is permitted via any means except with the instructor or TA.**\n",
    "- The project solution must be entirely your own independent work.\n",
    "- You may ask questions to the instructor or TA via email or in person. He may ask you to ask the question on Piazza so that all students benefit equally from the answer.\n",
    "- Academic dishonesty will not be tolerated and anyone caught violating the above rules will be referred to Student Judicial Affairs.\n",
    "\n",
    "Instructions:\n",
    "\n",
    "\n",
    "- Use the notebook called `project.ipynb` for your solution. Submit that single notebook.\n",
    "- Use code cells to do computations and use markdown cells to write any prose style answers.\n",
    "- Do not include scratch work. Clean up the notebook into its essential parts and remove extraneous work.\n",
    "- Do not leave any Python errors in the notebook and ensure that the entire notebook executes before submitting (run \"kernel restart and run all\").\n",
    "- Make sure to backup your work. If the server goes down you are responsible for your missing work. Download your project periodically in case.\n",
    "- Submit your completed work via the assignments tab by 6 PM on the due date. Only submissions with timestamps before 6 PM will be graded.\n",
    "\n",
    "# Car Suspension Design for the Yolo Causeway\n",
    "\n",
    "![](https://upload.wikimedia.org/wikipedia/commons/1/17/Yolo_Causeway_circa_1920.jpg)\n",
    "\n",
    "The old [Yolo Causeway](https://en.wikipedia.org/wiki/Yolo_Causeway) (pre-1962) was constructed by simply supported beams across 30 meter spans. These beams deflected due to their own weight with a maximum deflection at the center of the spans of 7 centimeters. This design worked well for cars of the era, but as cars improved over time and speeds increased it was discovered that at certain speeds, some cars vibrated to both uncomfortable and unsafe levels.\n",
    "\n",
    "You work for a major car manufacturer at the time of this discovery and management is very concerned that the suspension designs of the past era will no longer cut it for higher travel speeds with new car models. Management has put together a team of engineers to solve this problem. You are part of the suspension design team. You have been provided with a Python (it would have actually be Fortran in ~1960) file, `half_car.py`, that contains a simulation model of a half car system. This model was developed by a previous engineer and does a very good job at modelling the existing car suspension. You can use this model to obtain various \"measurements\" from a typical car designed by your company driving over arbitrary road profiles at a range of speeds.\n",
    "\n",
    "**Your primary goal is to design a new suspension for the car so that it has suitable displacement, rotational, and force transmissibility ratios for speeds from 0 to 130 km/h while riding over the causeway.** You must decide what is suitable and explain why in your notebook.\n",
    "\n",
    "Develop a well organized and professionally presented Jupyter notebook with your findings. The sections should follow this format:\n",
    "\n",
    "1. Derivation and implementation of a mathematical model of the causeway road profile for use with the provided half car system. (implemented as measurements)\n",
    "2. Analysis and characterization of the nonlinear and linear simulation of the half car system traversing the causeway. Include a modal analysis. In particular, point out any speeds that are problematic. Also discuss any limitations of the model.\n",
    "3. Derivation (with Lagrange's method) and implementation of a linear quarter car model that represents the entire car. Choose either a single degree of freedom model or a two degree of freedom model (sprung and unsprung mass). Justify your choice of model.\n",
    "4. Demonstration that your model has quantitatively similar behavior as the provided half car models. Make use of modal analyses, frequency response analysis, and simluation here. Discuss any limitations of your model.\n",
    "5. Design and demonstration of a suspension design that meets the specifications using both the quarter car model and half car model riding over the causeway. Choosing appropriate constants for the half car model is sufficient.\n",
    "6. Discussion of the results that explain the rationale for your design choices and whether the quarter car model is sufficient to design the suspension for the half car system. Specifically address the transmissibility ratios.\n",
    "\n",
    "Make use of plenty of graphics (don't forget labels, etc) to demonstrate the effectiveness of your design (animations are not required but may be helpful). This includes free body diagrams. You will be graded on whether your solution works and how well you demonstrate your command of the vibrations materials learned in this course.\n",
    "\n",
    "Tips:\n",
    "\n",
    "- Start simple and increase complexity as it is needed.\n",
    "- Explain the things you understand well completely first and then work on the other topics.\n",
    "- Simply supported beam deflection can be described by a 4th order polynomial, check your 150A book.\n",
    "- Do not assume that the reader knows the details of your work. It should be targeted to an engineering management audience, i.e. a techinically competent manager that likely had a vibrations course in engineering school.\n",
    "- If you want to animate your quarter car model here is the code for animating the quarter car model we used in class: https://github.com/moorepants/resonance/blob/master/resonance/linear_systems.py#L1533\n",
    "- Feel free to investigate the `half_car.py` file to see how the half car system was implemented.\n",
    "- Rotational transmissibily might be defined as comparing what rotation amplitudes occur if the stiffness of the suspension where inifinite to the rotation amplitude of with the stiffness you choose."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Demonstration of using the half car system\n",
    "\n",
    "## Free body diagram\n",
    "\n",
    "Here is a schematic showing the important variables for the provided half car model.\n",
    "\n",
    "![](half-car.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generalized Coordinates\n",
    "\n",
    "- $\\theta$: The angular rotation of the car.\n",
    "- $y_C$: Vertical distance from the reference to the sprung mass center.\n",
    "- $y_{Rs}$: The vertical distance from the reference to the rear unsprung mass.\n",
    "- $y_{Fs}$: The vertical distance from the reference to the front unsprung mass.\n",
    "\n",
    "## Extra (dependent) coordinates\n",
    "\n",
    "- $y_{Rs}, y_{Fs}$: The vertical distance from the reference to the top of the rear and front supension.\n",
    "- $x_R, x_F$: The longitudinal coordinates of the rear and front wheels.\n",
    "\n",
    "## Constants\n",
    "\n",
    "- $I_c$ [$kg \\cdot m^2$]: Rotational inertia of the sprung mass (the car) about it's mass center.\n",
    "- $m_C$ [kg]: Mass of the sprung mass (car, engine, people, etc).\n",
    "- $m_R$ [kg]: Mass of the rear unspurng mass (axle, wheels, etc).\n",
    "- $m_F$ [kg]: Mass of the front unsprung mass (axle, wheels, etc).\n",
    "- $l$ [m]: Distance between the top supension connection points.\n",
    "- $a$ [m]: The distance along a line connecting the top of the two suspension points to the mass center of the sprung mass.\n",
    "- $b$ [m]: The perpendicular distance from the line connecting the top of the suspension points to the mass center of the sprung mass.\n",
    "- $c_T$ [$kg \\cdot s$]: Linear damping coefficient of the tire.\n",
    "- $c_{Rs}$ [$kg \\cdot s$]: Linear damping coefficient of the rear suspension.\n",
    "- $c_{Fs}$ [$kg \\cdot s$]: Linear damping coefficient of the front suspension.\n",
    "- $k_T$ [N/m]: Linear spring coefficient of the tire (tire stiffness).\n",
    "- $k_{Rs}$ [N/m]: Linear spring coefficient of the rear suspension.\n",
    "- $k_{Fs}$ [N/m]: Linear spring coefficient of the front suspension.\n",
    "- $d_w$ [m]: Diameter of the wheel. The radius of the wheel is the free length of the tire spring.\n",
    "- $l_{so}$ [m]: Free length of the front and rear suspension springs.\n",
    "- $g$ [$m/s^2$]: Acceleration due to gravity.\n",
    "- $v$ [m/s]: The forward speed of the car.\n",
    "- $d$ [m]: The distance between the default road profile sinusoidal peaks.\n",
    "- $Y$ [m]: The amplitude of the default road profile sinusoid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from half_car import HalfCarSystem"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys = HalfCarSystem()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.coordinates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.speeds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.constants"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Setting the initial conditions\n",
    "\n",
    "For a given road profile and set of spring/mass constants you can set the initial conditions using the following function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.set_coordinates_to_equilibrium()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This puts the car into a configuration such that the suspension springs are compression to some value less than their free lengths. You should do this after changing and of the system's constants."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.coordinates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.plot_configuration();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you make the car heavier in the rear, then the initial conditions will show it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.constants['a'] = 0.3\n",
    "sys.set_coordinates_to_equilibrium()\n",
    "sys.plot_configuration();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simulation and animation\n",
    "\n",
    "Once all the constants, coordinates, and speeds are set, simulate the motion with `free_response()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys = HalfCarSystem()  # reset system\n",
    "traj = sys.free_response(5.0, sample_rate=200)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.animate_configuration(fps=60)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining the road\n",
    "\n",
    "The road is defined by several measurements:\n",
    "\n",
    "- $y_R, y_F$: The vertical coordinates of the road at the rear and front wheels.\n",
    "- $v_r, v_F$: The vertical velocity of the road at the rear and front wheels contact points.\n",
    "\n",
    "You will need to delete the measurement functions (e.g. `del sys.measurements['yR']`) and replace them with measurement functions that implement the Yolo Causeway road. Feel free to investigate the default measurement functions in `half_car.py`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.measurements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A sinusoidal road is implemented already and you can see how it behaves. Here is an example of driving over big bumps:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys = HalfCarSystem()  # reset system\n",
    "sys.constants['Y'] = 0.1\n",
    "sys.constants['d'] = 1.8\n",
    "traj = sys.free_response(1.0, sample_rate=1000)  # higher sample rate need for more frequent bumps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "traj[['yR', 'vR', 'yF', 'vF']].plot(subplots=True);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.animate_configuration(fps=60)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear system\n",
    "\n",
    "There is also a linear system which you can use to get the mass, damping and stiffness matrices with respect to linearization about the equilibrium point on a flat road.  **Note that the forcing function has not been setup here so the simulation will not respond to the road. Use the nonlinear system to simulate traversing the road.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from half_car import LinearHalfCarSystem"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lin_sys = LinearHalfCarSystem()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M, C, K = lin_sys.canonical_coefficients()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "C"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "K"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
