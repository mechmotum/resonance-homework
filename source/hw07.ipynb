{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-cdfd8a5fc23b3e53",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-bcf43d0f42e2f874",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Problem 1\n",
    "\n",
    "Below is a simple model of a runner's ponytail. It is a compound pendulum (slender rod) that pivots about its end which is displaced vertically by the person's head while they run, $a(t) = A \\sin \\omega_b t$. If the angle $\\theta$ is zero, the vertical excitations alone won't cause the pendulum to swing in the horizontal direction. However, if it is perturbed by some initial angle $\\theta_0$, the system can oscillate or even exhibit unstable behavior.\n",
    "\n",
    "![](ponytail.svg)\n",
    "\n",
    "Read the first two sections of [this paper](http://epubs.siam.org/doi/pdf/10.1137/090760477) which describes this rigid rod model and discusses some of its behavior. It provides the equation of motion for the system, which is:\n",
    "\n",
    "$$\n",
    "\\ddot{\\theta} + \\frac{2}{L}\\left(g + \\ddot{a}\\right)\\sin\\theta = 0\n",
    "$$\n",
    "\n",
    "Implement the nonlinear model using a `SingleDoFNonLinearSystem`. For a length $L = 25~\\mathrm{cm}$, and a \"bounce\" amplitude $A = 10~\\mathrm{cm}$, find a running cadence ($\\omega_b$) that causes unsteady behavior and one that does not. Plot at least $\\theta(t)$ in both cases.\n",
    "\n",
    "For the steady behavior, make a plot of the 2D position of the center of mass, i.e. plot the vertical position of the center of mass on the y axis and the horizontal position of the center of mass on the x axis.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "problem-01",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we can import the `SingleDoFNonLinearSystem` and set up its constants and speeds according to the prompt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from resonance.nonlinear_systems import SingleDoFNonLinearSystem\n",
    "\n",
    "sys = SingleDoFNonLinearSystem()\n",
    "sys.constants['L'] = 0.25\n",
    "sys.constants['A'] = 0.10\n",
    "sys.constants['g'] = 9.81\n",
    "\n",
    "# small perturbation angle\n",
    "sys.coordinates['theta'] = np.deg2rad(1)\n",
    "sys.speeds['omega'] = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll also need the excitation frequency as a system constant, so for now we can set it to the running cadence given in the paper which causes the unsteady behavior: $169~\\mathrm{steps/min}$ which is approximately $\\omega_b = 17.71~\\mathrm{rad/s}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.constants['wb'] = 17.71"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can write a function that computes the displacement function $a(t)$ and its second derivative $\\ddot{a}(t)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def a(A, wb, time):\n",
    "    return A * np.sin(wb * time)\n",
    "\n",
    "def addot(A, wb, time):\n",
    "    return -A * wb**2 * np.sin(wb * time)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can implement the right hand side of the system of differential equations. We use the usual substitutions $\\dot{\\theta} = \\omega$ and $\\dot{\\omega} = \\ddot{\\theta}$ to turn the single second order ODE into a pair of first order ODEs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def rhs(theta, omega, time, L, A, g, wb):\n",
    "    theta_dot = omega\n",
    "    omega_dot = -2 / L * (g + addot(A, wb, time)) * np.sin(theta)\n",
    "    return theta_dot, omega_dot\n",
    "\n",
    "sys.diff_eq_func = rhs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can simulate the free response and plot the trajectories."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "traj = sys.free_response(10)\n",
    "traj.plot(subplots=True);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that $\\theta$ does oscillate somewhat, but it grows in time. This is the unsteady behavior discussed in the paper. If we move $\\omega_b$ away from twice the natural frequency of the pendulum, we can find steady oscillatory behavior. We also want to plot the position of the center of mass, so we'll add those measurements now. The horizontal position is just $\\frac{L}{2}\\sin\\theta$. The vertical position (we can choose whatever reference we want) is $a(t) - \\frac{L}{2}\\sin\\theta$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def x_com(theta, L):\n",
    "    return L/2*np.sin(theta)\n",
    "\n",
    "def y_com(theta, L, A, wb, time):\n",
    "    return a(A, wb, time) - L/2*np.sin(theta)\n",
    "\n",
    "sys.add_measurement('x_com', x_com)\n",
    "sys.add_measurement('y_com', y_com)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now setting the running cadence to something much lower and simulating again, we can see the steady behavior."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.constants['wb'] = 17.71 / 3\n",
    "\n",
    "traj = sys.free_response(10)\n",
    "traj.plot(subplots=True);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can plot the position of the center of mass, which makes a pleasing curve."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(traj.x_com, traj.y_com)\n",
    "ax.set_xlabel('horizontal position')\n",
    "ax.set_ylabel('vertical position');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-7b87f8c8bc0143b1",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Problem 2\n",
    "\n",
    "Consider a simple system where a block of mass $m$ hangs from the ceiling from a spring with stiffness $k$. We may choose any reference position we want to define the coordinate for the motion of this system.\n",
    "\n",
    "In the diagram below are two different choices for a coordinate. On the left, the unstretched spring is shown. In the middle, $x_u$ is shown to be the displacement measured from where the mass is positioned when the spring is not stretched. On the right, we use $x_s$ which is measured from the position of the mass if you let it hang statically from the spring (static deflection $\\delta$).\n",
    "\n",
    "![](spring-eq.svg)\n",
    "\n",
    "Use Newton's second law ($F = ma$) to derive an equation of motion for each coordinate (they are different).\n",
    "\n",
    "Which of the two is equivalent to a mass-spring system that is horizontal (i.e. no gravitational force)?\n",
    "\n",
    "Which reference point ($x_u = 0$ or $x_s = 0$) does the system oscillate symmetrically about?\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "problem-02",
     "locked": false,
     "points": 3,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Starting with the unstretched coordinate $x_u$, we have two forces acting on the mass: the force from gravity $mg$ and the force from the spring which is stretched an amount $x_u$.\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\sum F &= mg - kx_u \\\\\n",
    "       &= m \\ddot{x}_u\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "The equation of motion then is\n",
    "\n",
    "$$\n",
    "m\\ddot{x}_u + kx_u = mg\n",
    "$$\n",
    "\n",
    "Now for the stretched coordinate $x_s$, the force from gravity is the same, but this time the spring force is different. Now the spring is stretched an amount $x_s$ *plus* the stretch from hanging the mass statically from the spring. This static deflection is found where the gravitational force $mg$ balances with the spring force $k\\delta$ (where $\\delta$ is the static stretch). This means the static stretch is $\\delta = \\frac{mg}{k}$, and so the total stretch is $x_s + \\delta$.\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\sum F &= mg - k\\left(x_s + \\frac{mg}{k}\\right) \\\\\n",
    "       &= m \\ddot{x}_s\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Notice that the $mg$ terms cancel and we get the following equation of motion.\n",
    "\n",
    "$$\n",
    "m\\ddot{x}_s + kx_s = 0\n",
    "$$\n",
    "\n",
    "So now we see that when we choose a coordinate referenced to the static stretch of the spring, we recover an equation of motion with no constant forcing term, which is equivalent to the horizontal version with no gravity at all.\n",
    "\n",
    "This means the equilibrium position for the oscillations is $x_s = 0$.\n",
    "\n",
    "Note that the homogeneous solution to both equations is the same. The particular solution for $x_u$ accounts for this shift."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-c4069624df464644",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Problem 3\n",
    "\n",
    "The quarter car system that was discussed in class is based on the following model.\n",
    "\n",
    "![](quarter-car.svg)\n",
    "\n",
    "Here $y(t)$ is a known function of time (the road profile), and we'd like to know the motion of the car's body $x(t)$. Derive the equation of motion for this system, using $y(t) = Y\\sin\\omega_b t$. As pointed out in problem 2, it is helpful to define $x$ with respect to a reference position which is the static deflection caused by gravity.\n",
    "\n",
    "Use a `SingleDoFLinearSystem` to simulate the system. Note that you will need to collect terms in the equation of motion that do not contain $x$ or its derivatives on the right hand side as forcing input. Use `periodic_forcing_response` to simulate.\n",
    "\n",
    "Use the `SimpleQuarterCarSystem` with equivalent parameters to verify your solution. Note that `sinusoidal_base_displacing_response` may be simpler to use for the `SimpleQuarterCarSystem` than `periodic_forcing_response`.\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "problem-03",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assuming $x(t)$ is measured with respect to the system at rest (i.e. the car body is allowed to compress the spring to the equilibrium position), the spring will be stretched by a distance $x - y$. The velocity across the damper will similarly be $\\dot{x} - \\dot{y}$. Then summing the forces with the positive direction upward, we get:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\sum F &= -k(x - y) - c\\left(\\dot{x} - \\dot{y}\\right) \\\\\n",
    "       &= m \\ddot{x}\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Collecting all terms with $x$ and its derivatives on the left hand side and all other terms on the right hand side, we get the equation of motion in canonical form:\n",
    "\n",
    "$$\n",
    "m\\ddot{x} + c\\dot{x} + kx = c\\dot{y} + ky\n",
    "$$\n",
    "\n",
    "Plugging in the known $y(t)$ (and its time derivative), we get\n",
    "\n",
    "$$\n",
    "m\\ddot{x} + c\\dot{x} + kx = cY\\omega_b\\cos\\omega_bt + kY \\sin\\omega_bt\n",
    "$$\n",
    "\n",
    "The right hand side is the forcing function, which can be implemented as a simple Fourier series forcing function with $N = 1$. That is\n",
    "\n",
    "$$\n",
    "a_0 = 0\n",
    "$$\n",
    "\n",
    "$$\n",
    "a_1 = c Y \\omega_b\n",
    "$$\n",
    "\n",
    "$$\n",
    "b_1 = k Y\n",
    "$$\n",
    "\n",
    "Starting with a `SingleDoFLinearSystem`, we set up a coordinate and speed and some system constants."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from resonance.linear_systems import SingleDoFLinearSystem\n",
    "\n",
    "sys = SingleDoFLinearSystem()\n",
    "sys.coordinates['x'] = 0\n",
    "sys.speeds['v'] = 0\n",
    "\n",
    "sys.constants['m'] = 1000\n",
    "sys.constants['k'] = 10000\n",
    "sys.constants['c'] = 100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need to define the `canonical_coeffs_func`. In this case, the coefficients are trivially $m$, $c$, and $k$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def coeffs(m, c, k):\n",
    "    return m, c, k\n",
    "\n",
    "sys.canonical_coeffs_func = coeffs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can define the road input frequency and amplitude, then use the Fourier series coefficients above to find the forced response."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wb = 2 * np.pi * 2\n",
    "Y = 0.05\n",
    "\n",
    "traj = sys.periodic_forcing_response(\n",
    "    0, sys.constants['c']*Y*wb, sys.constants['k']*Y, wb, 30)\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(traj.index, traj.x)\n",
    "ax.set_xlabel('time (s)')\n",
    "ax.set_ylabel('position (m)');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To verify this, we can set up a `SimpleQuarterCarSystem` with equivalent mass, suspension parameters, and initial conditions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from resonance.linear_systems import SimpleQuarterCarSystem\n",
    "\n",
    "car = SimpleQuarterCarSystem()\n",
    "car.constants['sprung_mass'] = sys.constants['m']\n",
    "car.constants['suspension_damping'] = sys.constants['c']\n",
    "car.constants['suspension_stiffness'] = sys.constants['k']\n",
    "car.coordinates['car_vertical_position'] = sys.coordinates['x']\n",
    "car.speeds['car_vertical_velocity'] = sys.speeds['v']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now we can simulate the `SimpleQuarterCarSystem` using `sinusoidal_base_displacing_response`, which takes the amplitude $Y$ and the frequency $\\omega_b$ and does the hard work of generating the correct forcing function for us."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "traj = car.sinusoidal_base_displacing_response(Y, wb, 30)\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(traj.index, traj.car_vertical_position)\n",
    "ax.set_xlabel('time (s)')\n",
    "ax.set_ylabel('position (m)');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looking at the two plots, you can see that the trajectories are the same."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
