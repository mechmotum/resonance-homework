{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Python\n",
    "\n",
    "This assignment is a short introduction to some of the core concepts used in future classwork and homework notebooks. Read through each section and execute the cells, making sure you understand how the code in the code cells generates the output you see. Once you've gone through a section, complete the corresponding exercise in the provided empty cells.\n",
    "\n",
    "**Important**: once you're finished, go to the \"Kernel\" menu and select \"Restart and Run All\" to make sure everything in the notebook executes properly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Importing Packages\n",
    "\n",
    "In Python, there are only a few core features that are accessible by default. The power of the language comes from packages. You can make use of these packages by importing them.\n",
    "\n",
    "The following cell imports a package called [NumPy](http://www.numpy.org/) and makes its functionality available under the shorthand name `np`. NumPy is a package that provides some functionality for working with data in arrays, like vectors and matrices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NumPy has some functions that may look familiar to MATLAB users (See [NumPy for Matlab Users](https://docs.scipy.org/doc/numpy-dev/user/numpy-for-matlab-users.html) for a good reference). For example, the `linspace` function takes a starting point, a stopping point, a number of points between, and generates an array of linearly spaced values accordingly.\n",
    "\n",
    "Note that you can access the documentation for the function by typing `np.linspace?`. The `?` appended to any function will open a help dialog."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.linspace(0, 1, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also create arrays manually by specifying every element inside square brackets like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.array([0.5, -1.0, 2.0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another package we'll be using this quarter is matplotlib. We'll most often import matplotlib like this. It imports matplotlib's `pyplot` package with the shorthand name `plt`. pyplot offers a somewhat MATLAB-like plotting interface."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll also use a special kind of command in notebooks that make interactive plots show up in the notebook. This command is specific to Jupyter notebooks and is not normal Python code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `plot` function of `pyplot` takes some x coordinates in an array and corresponding y coordinates in an array. Let's build an array of x values that range from 0 to 1, an array of y values from 0 to 100, and plot them. Note that the *size* of the arrays must match. With `linspace`, we directly specify the size with the third argument."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(np.linspace(0, 1, 10), np.linspace(0, 100, 10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that you can suppress the `[<matplotlib.lines.Line2D at 0x7fdb80df7940>]` by appending a `;`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(np.linspace(0, 1, 10), np.linspace(0, 100, 10));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###  Exercise 1\n",
    "\n",
    "In the code cell below, re-create the plot we just made, but use red circles instead of a blue line.\n",
    "\n",
    "You may find matplotlib's documentation helpful: http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-10cfb9f33ed63c11",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "plt.plot(np.linspace(0, 1, 10), np.linspace(0, 100, 10), 'ro')\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Arrays\n",
    "\n",
    "Arrays wouldn't be very useful if you couldn't do any mathematical operations with them. NumPy arrays naturally support arithmetic like elementwise scalar multiplication and addition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "2 * np.linspace(0, 1, 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.linspace(0, 1, 5) + 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also add array elements together, assuming the arrays are compatibly sized (we'll be a bit more specific about what is meant by \"compatibly sized\" later on in the class)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.array([1, 2, 3]) + np.array([4, 5, 6])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NumPy arrays all have a specific number of dimensions. All of the arrays we've seen so far have been 1-dimensional, which you might call a vector. A 1D array is just a list of numbers. A 2D array, then, can be thought of as a list of lists of numbers. Here's how you can build one manually specifying all of the entries.\n",
    "\n",
    "Notice that the syntax looks like we take two lists of numbers and put them inside another list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.array([[1, 2, 3], [4, 5, 6]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 2\n",
    "\n",
    "Create an array $\\begin{bmatrix} 0 & 3 \\\\ 2 & 5 \\\\ 1 & 1 \\end{bmatrix}$ and add it to an array created by the expression `np.ones(2)` in the code cell below. Explain how the output results from these inputs.\n",
    "\n",
    "*Hint*: if you're curious what `np.ones(3)` does, you can write `np.ones?` in a code cell and execute it to see the function's documentation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-a27584c76f397f18",
     "locked": false,
     "points": 0.5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "np.array([[0, 3], [2, 5], [1, 1]]) + np.ones(2)\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-e8e8beb732eb5f73",
     "locked": false,
     "points": 0.5,
     "schema_version": 1,
     "solution": true
    }
   },
   "source": [
    "Replace this text with your explanation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Variables\n",
    "\n",
    "If you want to give something a name and make it usable later on in your code, you can do so with the equals sign (`=`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "x = np.array([0, 2, 8])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that since we have assigned the array to a variable called `x`, it doesn't get printed out when the cell is executed. If you want the array to be printed, you can leave the variable on a line on its own in a cell or you can use Python's built-in `print()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# some code here, this is just a comment that has no effect\n",
    "x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can do operations on the array using the name `x`.\n",
    "\n",
    "Scalar addition:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x + 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Array (elementwise) addition:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x + x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Exponentiation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x**2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you re-use the name `x`, the contents are just overwritten."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.array([0, np.pi/2, np.deg2rad(60)])\n",
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note**: be careful with re-using names in Jupyter notebooks and running cells out of order. Now that `x` has been overwritten, if you rerun the cell above containing `x**2`, the output *will change* to reflect the squares of the elements in *the new* array called `x`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 3\n",
    "\n",
    "Imagine that the values in `x` are angles measured in radians. Compute $\\cos(x_i)$ for each element $x_i$ in the array $\\mathbf{x}$, and make sure the results are visible in the output of the cell below. Are the outputs what you expect? You might find this page helpful: https://docs.scipy.org/doc/numpy/reference/routines.math.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-fda7e187e524f340",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "np.cos(x)\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions\n",
    "\n",
    "Functions are useful for giving a name to some operation that you want to re-use. It can be especially tedious to read and understand a chunk of code that performs many mathematical operations. Splitting some of these things out to functions can make code more readily understood and easier to debug.\n",
    "\n",
    "A Python function is started with the word `def`. Then we specify the function's name, and specify its arguments inside parentheses. Everything *inside* the function is indented by four spaces. Functions end when a line beginning with `return` is run.\n",
    "\n",
    "Here's a function that takes an argument `radius` and computes the area of a circle with that radius. In this case, the *Python* function has a straightforward mapping to a *mathematical* function $A(r) = \\pi r^2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def circle_area(radius):\n",
    "    area = np.pi * radius**2\n",
    "    return area"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you run the cell above, nothing is printed. However, the function is now available elsewhere in your notebook and you can use it just like we've been using NumPy functions (e.g. `np.linspace()`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = circle_area(2)\n",
    "a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions can also take multiple inputs--each one is just separated by a comma--and they are accessible inside the function just like normal variables. Let's implement a function that computes the volume of a cylinder, for which we'll need the cylinder radius and height $V = \\pi r^2 h$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def cylinder_volume(radius, height):\n",
    "    volume = np.pi * radius**2 * height\n",
    "    return volume\n",
    "\n",
    "cylinder_volume(2, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is fine, but notice that a cylinder's volume can also be written in terms of its cross-sectional area $V = Ah$, and we've already written a function that computes the area of a circle. This is a trivial example, but it illustrates the how useful functions can be when the \"sub-operations\" you use become larger and more complicated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def cylinder_volume_simpler(radius, height):\n",
    "    area = circle_area(radius)\n",
    "    volume = area * height\n",
    "    return volume\n",
    "\n",
    "cylinder_volume_simpler(2, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 4\n",
    "\n",
    "Write a function that computes the volume of a *hollow cylinder*\n",
    "\n",
    "<a title=\"By Ag2gaeh (Own work) [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], via Wikimedia Commons\" href=\"https://commons.wikimedia.org/wiki/File%3AZylinder-rohr-s.svg\"><img width=\"128\" alt=\"Zylinder-rohr-s\" src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Zylinder-rohr-s.svg/128px-Zylinder-rohr-s.svg.png\"/></a>\n",
    "\n",
    "given these dimensions in order:\n",
    "\n",
    "- the outer radius $R$\n",
    "- the wall thickness $b$\n",
    "- the height $h$\n",
    "\n",
    "See if you can implement this using the existing functions we've already implemented.\n",
    "\n",
    "Finally, use your function to compute the volume of a hollow cylinder with $R = 2$, $b = 1$, and $h = 10$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-acd7987ad65a8a9a",
     "locked": false,
     "points": 2,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "def hollow_cylinder_volume(outer_radius, thickness, height):\n",
    "    inner_radius = outer_radius - thickness\n",
    "    volume = cylinder_volume(outer_radius, height) - cylinder_volume(inner_radius, height)\n",
    "    return volume\n",
    "\n",
    "hollow_cylinder_volume(2, 1, 10)\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dictionaries\n",
    "\n",
    "Arrays are ordered lists of numbers. You can access individual elements of an array using an element's *index*, starting with zero."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.array([5, 3, 9])\n",
    "x[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x[2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes you don't really care about the ordering and you'd rather store elements according to an easy-to-remember name or identifier. Dictionaries are useful in this case. Dictionaries are created by surrounding the entries with curly brackets (`{` and `}`). Since there is no implicit index, you need to specify the *keys* yourself."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "profile = {'name': 'John', 'age': 22, 'email': 'john@domain.com'}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Splitting things up and giving each entry its own line may make the syntax a bit clearer. The dictionary below contains the exact same information as above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "profile = {\n",
    "    'name': 'John',\n",
    "    'age': 22,\n",
    "    'email': 'john@domain.com'\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can grab the piece of information you want using a key."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "profile['age']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "profile['email']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also add new entries to a dictionary fairly naturally."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "profile['city'] = 'Davis'\n",
    "profile"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 5\n",
    "\n",
    "Just above we saw how to add new entries to a dictionary. What happens if you try to add an entry using a key that already exists in the dictionary? Add an entry to `profile` using one of the keys that has already been used (e.g. `'age'` or `'email'`). Explain what happens to profile."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-d4d2c919be8acdc8",
     "locked": false,
     "points": 0.5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "profile['age'] = 23\n",
    "profile\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-f65ca9952db0c566",
     "locked": false,
     "points": 0.5,
     "schema_version": 1,
     "solution": true
    }
   },
   "source": [
    "Replace this text with your explanation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Systems\n",
    "\n",
    "In this class, we'll use and sometimes create systems which are computational representations of some mechanical system. For example, there is the `BookOnCupSystem`. We can get access to this system by importing it. Here, a slightly different form of import from what we've seen with NumPy and matplotlib is used--we'll import the system itself directly rather than the package it is contained in."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from resonance.linear_systems import BookOnCupSystem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can create this system like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sys = BookOnCupSystem()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you don't know what a system (or really anything in Python) provides, there are a few things you can do to find out. One is to type the name, add a period (`.`), then press the \"Tab\" key to see a pop-up list. Go ahead and try that below by typing `sys.` then pressing \"Tab.\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another useful and more detailed way to look up what you can do with the system is to type the name, add a question mark (`?`), then run the cell. Go ahead and run the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sys?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The page that pops up lists some documentation for the `BookOnCupSystem`. Notice that this system has an attribute (like a variable) called `constants`. We access this attribute as follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.constants"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Does this output look familiar? It looks like a dictionary, doesn't it?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 6\n",
    "\n",
    "Set the thickness of the `BookOnCupSystem` to `0.01` and leave the rest of the constants the same, then output all of the system constants again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-6230da5da8d70600",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "sys.constants['thickness'] = 0.01\n",
    "sys.constants\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## DataFrames\n",
    "\n",
    "pandas is a Python package that offers a very useful data container, called a dataframe, that works somewhat like a blend of a dictionary and a NumPy array. It can be useful to think of a dataframe as a table, with entries in rows, and named columns.\n",
    "\n",
    "We'll start by importing the pandas package with the shorthand name `pd`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a few ways to create a dataframe. One way is to create a dictionary of arrays. Here's an example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d = {\n",
    "    'column_a': np.array([1, 2, 3]),\n",
    "    'column_b': np.array([9.0, 8.2, 10.4])\n",
    "}\n",
    "d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we pass this dictionary to `pd.DataFrame` to create the dataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.DataFrame(d)\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that Jupyter notebooks display dataframes in a nice tabular format.\n",
    "\n",
    "You can access a column of a dataframe much like the dictionary we used to create it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df['column_b']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also plot the columns of a dataframe fairly easily."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Some More NumPy Functions\n",
    "\n",
    "NumPy provides a number of functions for working with arrays. The mathematical functions NumPy implements are listed here: https://docs.scipy.org/doc/numpy/reference/routines.math.html\n",
    "\n",
    "We saw above that there are things like trigonometric functions that will calculate some value for each element of an array. Other examples are things like exponentials and logarithms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "x = np.array([4.2, 1.2, 0.1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.exp(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.log(np.exp(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are also functions that operate on an entire array all at once, rather than elementwise (element-by-element)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.sum(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 7\n",
    "\n",
    "The root mean square (RMS) is commonly used as a measurement of a vibration. It is defined as\n",
    "\n",
    "$$\n",
    "\\mathrm{RMS}(\\mathbf{x}) = \\sqrt{ \\frac{1}{N} \\sum_{i=1}^N x_i^2 },\\;%\n",
    "    \\mathrm{where}\\; \\mathbf{x} = \\left[x_1, x_2,\\dots, x_N\\right]\n",
    "$$\n",
    "\n",
    "That is, it is the square root of the mean of the squares of the values of an array $\\mathbf{x}$. The function below returns an array representing a sinusoid with a period of 1 second and a given amplitude, like $x(t) = A \\sin(2\\pi t)$. Write a function to compute the RMS of an array and use it to determine the relationship between $A$ and $\\mathrm{RMS}(\\mathbf{x})$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sinusoid(amplitude):\n",
    "    return amplitude * np.sin(2*np.pi*np.arange(0, 1, 0.01))\n",
    "\n",
    "plt.plot(sinusoid(10))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-d0f2e6e69233b4df",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "def rms(x):\n",
    "    return np.sqrt(np.mean(np.square(x)))\n",
    "\n",
    "print(rms(sinusoid(1)))\n",
    "print(rms(sinusoid(10)) / 10)\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-7b87fb34678dcefb",
     "locked": false,
     "points": 1,
     "schema_version": 1,
     "solution": true
    }
   },
   "source": [
    "Replace this text with your description of the relationship between the sinusoid amplitude and the RMS value."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For further study we recommend the [SciPy Lecture Notes](http://www.scipy-lectures.org/). Going through this from the beginning will get you up to speed on the basics of scientific and engineering computation with Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Once you're finished, don't forget to go to Kernel -> Restart and Run All before submitting.**"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
