{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-d495ab4acfb33b46",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Airplane Wing Flutter\n",
    "\n",
    "A model of an aircraft wing with a fuel pod mounted at its tip is shown below. To make things simple, the wing is treated as a cantileavered beam  with length $l$, modulus of elasticity $E$, and cross-sectional moment of inertia $I$. This models the stiffness of the wing given a point load at the tip, which you'll need to determine (in $\\mathrm{N}/\\mathrm{m}$). The table here may help: https://mechanicalc.com/reference/beam-deflection-tables \n",
    "\n",
    "![](plane-wing-model.svg)\n",
    "\n",
    "We will also assume the wing has viscous damping that causes its vibrations to decay over time when the plane is at rest. So far, the model exhibits behavior similar to the pendulum with viscous damping. However, when the plane is flying, there are aerodynamic forces that depend on both the forward speed of the aircraft as well as the vibrational motion of the wing. Overall, this affects the `damping` constant of the system, which becomes $c - \\gamma$, where $c$ is the original damping constant of the wing (when the plane is at rest) and $\\gamma$ is a factor that indicates the plane's forward speed. A value of $\\gamma = 0$ indicates the aircraft is not moving, and a large value ($\\gamma \\gg 0$) indicates the aircraft is moving at a high speed.\n",
    "\n",
    "Use a `MassSpringDamperSystem` to investigate the behavior of this airplane wing model for various values of $\\gamma$. Some parameters you'll need are given below.\n",
    "\n",
    "- $I = 5.2 \\times 10^{-5}~\\mathrm{m}^4$\n",
    "- $E = 6.9 \\times 10^9~\\mathrm{N}/\\mathrm{m}^2$\n",
    "- $l = 2~\\mathrm{m}$\n",
    "- $m = 500~\\mathrm{kg}$\n",
    "- $c = 200~\\mathrm{kg}/\\mathrm{s}$\n",
    "\n",
    "Demonstrate what happens when the wing is released from some initial displacement while the plane is not moving. Then, identify the value of $\\gamma$ that causes the wing's oscillations to grow rather than decay. This behavior is called *flutter instability*. What about the system changes if the fuel pod is empty (i.e. set $m = 10~\\mathrm{kg}$)?\n",
    "\n",
    "Use multiple code and markdown cells to provide your answer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-0439df05cc1b073c",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "from resonance.linear_systems import MassSpringDamperSystem\n",
    "\n",
    "I = 5.2e-5\n",
    "E = 6.9e9\n",
    "l = 2\n",
    "m = 500\n",
    "k = 3 * E * I / l**3\n",
    "c = 200\n",
    "x0 = 0.1\n",
    "\n",
    "plane_wing = MassSpringDamperSystem()\n",
    "plane_wing.constants['mass'] = m\n",
    "plane_wing.constants['stiffness'] = k\n",
    "\n",
    "fig, axes = plt.subplots(3, 1, sharex=True)\n",
    "fig.tight_layout()\n",
    "for gam, desc, ax in zip([0, c, 2*c],\n",
    "                         ['not moving', 'critical speed', 'too fast'],\n",
    "                         axes):\n",
    "    plane_wing.constants['damping'] = c - gam\n",
    "    plane_wing.coordinates['position'] = x0\n",
    "    traj = plane_wing.free_response(10)\n",
    "    traj.position.plot(ax=ax, title=desc)\n",
    "\n",
    "# empty fuel pod\n",
    "plane_wing.constants['damping'] = c\n",
    "plane_wing.constants['mass'] = 10\n",
    "plane_wing.coordinates['position'] = x0\n",
    "traj = plane_wing.free_response(1, sample_rate=500)\n",
    "fig, ax = plt.subplots()\n",
    "traj.position.plot(title='empty pod', ax=ax)\n",
    "\n",
    "print(\"\"\"\n",
    "When not moving, the wing's oscillations slowly decay. When gamma\n",
    "is equal to c, the oscillations no long decay, and when it is larger\n",
    "than c, the oscillations grow (flutter instability).\n",
    "\n",
    "When the pod is empty, the oscillations are more rapid and decay faster.\n",
    "\"\"\")\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-da077b43235ef3e3",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# RMS Error\n",
    "\n",
    "Recall from homework 1 that the root mean square (RMS) is a useful measure of a signal's amplitude:\n",
    "\n",
    "$$\n",
    "\\mathrm{RMS}(\\mathbf{x}) = \\sqrt{ \\frac{1}{N} \\sum_{i=1}^N x_i^2 },\\;%\n",
    "    \\mathrm{where}\\; \\mathbf{x} = \\left[x_1, x_2,\\dots, x_N\\right]\n",
    "$$\n",
    "\n",
    "It is also, however, useful as a measure of how poorly a fitting operation has performed. That is, if we have some data that we fit a curve to, the RMS error (RMSE) tells us on average how far away the data points are from the fitted curve. It is just the RMS of the *error* (difference) between the data points and the corresponding points on the fitted curve. In this case, it looks like:\n",
    "\n",
    "$$\n",
    "\\mathrm{RMSE} = \\sqrt{ \\frac{1}{N} \\sum_{i=1}^N \\left(\\hat{x}_i - x_i\\right)^2 }\n",
    "$$\n",
    "\n",
    "where $x_i$ are our experimental measurements and $\\hat{x}_i$ are the corresponding values on the fitted curve.\n",
    "\n",
    "\n",
    "Create two different systems: a linear (viscously damped) `ClockPendulumSystem` and a nonlinear (Coulomb damped) `ClockPendulumSystem`. Leave all of the constants as their default values except give them both some damping. Make sure that the Coulomb damped system oscillates throughout the free response. Fit a curve of the form $x(t) = A e^{\\lambda t} \\cos \\left( \\omega t + \\phi\\right)$ to each one, plot both the free response and the fitted curve, and report the RMSE. How is the RMSE affected by the coefficient of friction in the Coulomb-damped system?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-c9b6503697b33cc1",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "from resonance.nonlinear_systems import ClockPendulumSystem as CoulombPendulum\n",
    "from resonance.linear_systems import ClockPendulumSystem as ViscousPendulum\n",
    "from scipy.optimize import curve_fit\n",
    "\n",
    "def decaying_sinusoid(times, amp, lam, freq, phase):\n",
    "    return amp * np.exp(lam*times) * np.cos(freq*times + phase)\n",
    "\n",
    "def rmse(measurements, predictions):\n",
    "    return np.sqrt(np.mean((predictions - measurements)**2))\n",
    "\n",
    "\n",
    "fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)\n",
    "\n",
    "theta0 = np.deg2rad(5)\n",
    "\n",
    "# viscous pendulum\n",
    "vpend = ViscousPendulum()\n",
    "vpend.constants['viscous_damping'] = 0.1\n",
    "vpend.coordinates['angle'] = theta0\n",
    "vpend_traj = vpend.free_response(5)\n",
    "\n",
    "popt_vpend, pcov = curve_fit(decaying_sinusoid,\n",
    "                             vpend_traj.index, vpend_traj.angle,\n",
    "                             p0=(1, -1, 1, 0))\n",
    "pred = decaying_sinusoid(vpend_traj.index, *popt_vpend)\n",
    "err = rmse(vpend_traj.angle, pred)\n",
    "ax1.plot(vpend_traj.index, vpend_traj.angle, label='response')\n",
    "ax1.plot(vpend_traj.index, pred, label='fit')\n",
    "ax1.legend()\n",
    "ax1.set_title('viscous system: RMSE {:.6f}'.format(err))\n",
    "\n",
    "# coulomb pendulum\n",
    "cpend = CoulombPendulum()\n",
    "cpend.constants['coeff_of_friction'] = 0.001\n",
    "cpend.coordinates['angle'] = theta0\n",
    "cpend_traj = cpend.free_response(5)\n",
    "\n",
    "popt_cpend, pcov = curve_fit(decaying_sinusoid,\n",
    "                             cpend_traj.index, cpend_traj.angle,\n",
    "                             p0=(1, -1, 1, 0))\n",
    "pred = decaying_sinusoid(cpend_traj.index, *popt_cpend)\n",
    "err = rmse(cpend_traj.angle, pred)\n",
    "ax2.plot(cpend_traj.index, cpend_traj.angle, label='response')\n",
    "ax2.plot(cpend_traj.index, pred, label='fit')\n",
    "ax2.legend()\n",
    "ax2.set_title('coulomb system: RMSE {:.6f}'.format(err))\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-636443ebdbb1a2e1",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Coulomb Damping Decay\n",
    "\n",
    "Use the clock pendulum system with Coulomb damping to determine the way that the oscillations of a vibratory system with friction decay. Set the system constants such that the free response oscillates at least 10 times. Write a function to determine the time and amplitude of the first 10 peaks in the free response, then plot these peaks as circles on top of the free response plot. Calculate the amplitude difference between successive peaks. What does this tell you about the envelope of the system's free response? How is it different from the equivalent system with viscous damping instead of friction?\n",
    "\n",
    "Next, set the coefficient of friction to 0.01, then plot the free response for at least 10 different initial angles on the same figure (zero initial velocity). Note that the system stops oscillating at a different angle in each case. What is the approximate range of the \"sticking region?\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-43a52692faded8e1",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "from resonance.nonlinear_systems import ClockPendulumSystem\n",
    "\n",
    "def find_peaks(time, amplitude, n=10):\n",
    "    peak_ind = np.where(np.diff(np.sign(np.diff(amplitude))) == -2)[0][:n]\n",
    "    return time[peak_ind], amplitude[peak_ind]\n",
    "\n",
    "cpend = ClockPendulumSystem()\n",
    "cpend.constants['coeff_of_friction'] = 0.002\n",
    "cpend.coordinates['angle'] = np.deg2rad(15)\n",
    "cpend.integrator = 'RK45'\n",
    "traj = cpend.free_response(15)\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "traj.angle.plot(ax=ax)\n",
    "\n",
    "peak_times, peak_amps = find_peaks(traj.index.values, traj.angle.values)\n",
    "ax.plot(peak_times, peak_amps, 'go')\n",
    "\n",
    "diffs = np.diff(peak_amps)\n",
    "print(\"Differences between successive amplitudes\")\n",
    "print(diffs)\n",
    "print(\"\"\"\n",
    "The differences are essentially all the same, indicating that\n",
    "the free response envelope decays linearly. This differs from\n",
    "the viscously damped system which decays exponentially.\n",
    "\"\"\")\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "cpend.constants['coeff_of_friction'] = 0.01\n",
    "max_sticking_amp = 0\n",
    "for init in np.deg2rad(np.linspace(0, 20, 10)):\n",
    "    cpend.coordinates['angle'] = init\n",
    "    traj = cpend.free_response(5)\n",
    "    deg = np.rad2deg(traj.angle)\n",
    "    deg.plot(ax=ax)\n",
    "    max_sticking_amp = max(max_sticking_amp, np.abs(deg.iloc[-1]))\n",
    "print(\"The sticking region ranges from +/- {:.3f} degrees\".format(max_sticking_amp))\n",
    "### END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Linearized Systems\n",
    "\n",
    "The `ClockPendulumSystem` in `resonance.nonlinear_systems` not only implements Coulomb friction, but it actually implements a more accurate version of the undamped pendulum system when the friction coefficient is left at zero. The `ClockPendulumSystem` in `resonance.linear_systems` uses what's called the *small angle approximation* which, as the name implies, assumes the pendulum oscillates with a small amplitude. Use the two systems with no damping or friction to investigate how good the linearized system approximates the more accurate model for different oscillation amplitudes.\n",
    "\n",
    "First, plot the free response of the two systems given a large initial angle (i.e. greater than 30 degrees). What is different between the two responses?\n",
    "\n",
    "Next, plot the root mean square error (RMSE) between the nonlinear and linear systems versus initial angle for a range of initial angles, from very small to large. For what oscillation amplitudes would you consider the linear system a good enough approximation?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "cell-51f1fccb3bffdbc7",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": [
    "### BEGIN SOLUTION\n",
    "from resonance.nonlinear_systems import ClockPendulumSystem as CoulombPendulum\n",
    "from resonance.linear_systems import ClockPendulumSystem as ViscousPendulum\n",
    "\n",
    "def rmse(measurements, predictions):\n",
    "    return np.sqrt(np.mean((predictions - measurements)**2))\n",
    "\n",
    "vpend = ViscousPendulum()\n",
    "cpend = CoulombPendulum()\n",
    "\n",
    "# plot the free response for each system\n",
    "cpend.coordinates['angle'] = np.deg2rad(50)\n",
    "vpend.coordinates['angle'] = np.deg2rad(50)\n",
    "cpend_traj = cpend.free_response(10)\n",
    "vpend_traj = vpend.free_response(10)\n",
    "fig, ax = plt.subplots()\n",
    "cpend_traj.angle.plot(ax=ax, label='nonlinear')\n",
    "vpend_traj.angle.plot(ax=ax, label='linear')\n",
    "ax.legend()\n",
    "print(\"\"\"\n",
    "The responses start out the same, but they eventually\n",
    "grow out of sync. The nonlinear system has a slightly\n",
    "larger period than the linear system.\n",
    "\"\"\")\n",
    "\n",
    "# plot RMSE vs. initial angle\n",
    "initial_angles = np.deg2rad(np.linspace(0, 50, 20))\n",
    "rmse_vals = np.zeros_like(initial_angles)\n",
    "\n",
    "for i, init in enumerate(initial_angles):\n",
    "    cpend.coordinates['angle'] = init\n",
    "    vpend.coordinates['angle'] = init\n",
    "    cpend_traj = cpend.free_response(5)\n",
    "    vpend_traj = vpend.free_response(5)\n",
    "    rmse_vals[i] = rmse(cpend_traj.angle, vpend_traj.angle)\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(np.rad2deg(initial_angles), np.rad2deg(rmse_vals))\n",
    "ax.set_xlabel('initial angle (deg)')\n",
    "ax.set_ylabel('RMSE (deg)')\n",
    "print(\"\"\"\n",
    "The RMSE is still pretty close to 0 degrees with\n",
    "an initial angle of up to about 10 degrees. After that\n",
    "the RMSE grows rapidly.\n",
    "\"\"\")\n",
    "### END SOLUTION"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
