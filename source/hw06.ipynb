{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-19b6e91c30bd3609",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Instructions\n",
    "\n",
    "This homework assignment includes some significant modeling work, so you do not need to include everything (e.g. free body diagrams, equations) in the notebook. You may neatly hand-write these parts of the homework and hand them in at the beginning of class. Some parts of the problem do require some code and simulation, so you should answer those aspects of the problem using the notebook as normal.\n",
    "\n",
    "Treat this notebook as the main document, so make sure to reference any handwritten work here so someone else can understand your work and follow what you're doing.\n",
    "\n",
    "The cells provided just below the problem prompts are just for grading. Use as many cells as you need to answer the problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-90e9fe8d55fb32da",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-325f48d4476173a9",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Problem 1\n",
    "\n",
    "Pictured below is a slender rod of length $l$ and mass $m$ which pivots without friction about a point $O$. Two springs are connected a distance $d$ from the end of the rod. You may assume the springs are deflected linearly in the horizontal direction and apply their restoring force perpendicular to the rod.\n",
    "\n",
    "![](springy_pendulum.svg)\n",
    "\n",
    "1. Derive the full nonlinear equation of motion for this system, then use the small angle approxmation to find a linearized equation of motion.\n",
    "2. What is the minimum $k$ that ensures the linearized system stiffness is positive?\n",
    "3. Use the linear equation of motion to find the natural frequency $\\omega_n$.\n",
    "4. Use a `SingleDoFLinearSystem` from `resonance.linear_systems` to plot the system's free response to an initial angle when the stiffness is positive and when it is negative. Use whatever values for $m$, $l$, and $d$ that you want.\n",
    "5. Verify that your expression for $\\omega_n$ is correct by comparing to the simulated system's period.\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "problem-01",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Nonlinear Equation of Motion**\n",
    "\n",
    "Taking the sum of moments about the pivot point, there are the two springs acting in parallel (effectively doubling the stiffness), and there is gravity acting at the center of mass of the rod.\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\sum M_O &= mg\\frac{l}{2}\\sin\\theta - 2\\left(k(l-d) \\sin\\theta\\right)\\left((l-d)\\cos\\theta\\right) \\\\\n",
    "         &= I_O \\ddot{\\theta} \\\\\n",
    "         &= \\frac{ml^2}{3}\\ddot{\\theta}\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Then the equation of motion is:\n",
    "\n",
    "$$\n",
    "\\frac{ml^2}{3}\\ddot{\\theta} + \\left(2k\\left(l-d\\right)^2\\cos\\theta - mg\\frac{l}{2}\\right)\\sin\\theta = 0\n",
    "$$\n",
    "\n",
    "Using the small angle approximation ($\\sin\\theta \\approx \\theta$), the linearized equation of motion is:\n",
    "\n",
    "$$\n",
    "\\frac{ml^2}{3}\\ddot{\\theta} + \\left(2k(l-d)^2 - \\frac{mgl}{2}\\right) \\theta = 0\n",
    "$$\n",
    "\n",
    "**Critical Stiffness**\n",
    "\n",
    "To find the critical stiffness, we set the coefficient of $\\theta$ in the equation of motion to greater than zero and solve for $k$.\n",
    "\n",
    "$$\n",
    "k > \\frac{mgl}{4\\left(l-d\\right)^2}\n",
    "$$\n",
    "\n",
    "**Natural Frequency**\n",
    "\n",
    "The natural frequency is:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\omega_n &= \\sqrt{ \\frac{ k_{\\mathrm{eq}} }{ m_{\\mathrm{eq}} } } \\\\\n",
    "         &= \\sqrt{\\frac{12k(l-d)^2 - 3mgl}{2ml^2}}\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to use the `SingleDoFLinearSystem` to simulate this system, we just set the system mass and stiffness to $m_\\mathrm{eq}$ and $k_\\mathrm{eq}$, respectively, by setting the `canonical_coeffs_func`. For the parameters $m$, $l$, and $d$, we'll just use any reasonable values. We'll also set up the system with a single coordinate for $\\theta$ and a single speed for $\\dot{\\theta}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from resonance.linear_systems import SingleDoFLinearSystem\n",
    "\n",
    "m = 1\n",
    "g = 9.81\n",
    "l = 0.5\n",
    "d = 0.1\n",
    "\n",
    "sys = SingleDoFLinearSystem()\n",
    "sys.constants['mass'] = m\n",
    "sys.constants['gravity'] = g\n",
    "sys.constants['length'] = l\n",
    "sys.constants['attachment'] = d\n",
    "sys.constants['stiffness'] = 1  # we'll set this later\n",
    "\n",
    "sys.coordinates['angle'] = 0.1\n",
    "sys.speeds['velocity'] = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From these values, we can compute the critical stiffness $k_\\mathrm{c} = \\frac{mgl}{4\\left(l-d\\right)^2}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k_crit = m * g * l / (4 * (l-d)**2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can set up the system and plot the free response for $k_\\mathrm{eq}$ just greater than and just less than $k_\\mathrm{c}$. In this case, it is better to use separate axes since the unstable system's response grows very large quickly and we don't want the scaling to hide what happens for the stable system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def coeffs(mass, stiffness, gravity, length, attachment):\n",
    "    meq = mass * length**2 / 3\n",
    "    keq = 2 * stiffness * (length - attachment)**2 - \\\n",
    "        mass * gravity * length/2\n",
    "    return meq, 0, keq\n",
    "\n",
    "sys.canonical_coeffs_func = coeffs\n",
    "\n",
    "# stiffness values just below and just above the critical value\n",
    "k_vals = [0.9 * k_crit, 1.1 * k_crit]\n",
    "\n",
    "fig, axes = plt.subplots(2)\n",
    "\n",
    "# zip lets us iterate over multiple containers at once\n",
    "for k, ax in zip(k_vals, axes):\n",
    "    sys.constants['stiffness'] = k\n",
    "    sys.coordinates['angle'] = 0.1\n",
    "    traj = sys.free_response(5)\n",
    "    ax.plot(traj.index, traj.angle)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now to estimate the period, we can just use the system's `period` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.period()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we can verify that our expression above for $\\omega_n$ is correct"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wn = np.sqrt((12*k*(l-d)**2 - 3*m*g*l)/(2*m*l**2))\n",
    "T = 2 * np.pi / wn\n",
    "T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-3695bcdb0d0e8617",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Problem 2\n",
    "\n",
    "Derive the equation of motion for the book balancing on a cylindrical cup.\n",
    "\n",
    "Linearize the equation of motion using the small angle approximation, and state the specific assumptions required to get a linear equation of motion (e.g. like the usual $\\sin\\theta \\approx \\theta$).\n",
    "\n",
    "What is the stability criterion for the linearized system? Does this make intuitive sense?\n",
    "\n",
    "<img src=\"book-balance.svg\" width=300/>\n",
    "\n",
    "----"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "problem-02",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Equation of Motion**\n",
    "\n",
    "We'll use Langrange's method to solve this problem. Starting with the kinetic energy, note that we can use the traditional form:\n",
    "\n",
    "$$\n",
    "T = \\frac{1}{2}m v_O^2 + \\frac{1}{2} I_O \\omega^2\n",
    "$$\n",
    "\n",
    "where point $O$ is the center of the book. However, it is much easier to find the kinetic energy via the *contact point*, where the only term is\n",
    "\n",
    "$$\n",
    "T = \\frac{1}{2}I_C \\dot{\\theta}^2\n",
    "$$\n",
    "\n",
    "because the instantaneous velocity of the contact point is 0.\n",
    "\n",
    "We can find $I_C$ using the parallel axis theorem:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "I_C &= I_O + m\\left[\\left(\\frac{t}{2}\\right)^2 + \\left(r\\theta\\right)^2\\right] \\\\\n",
    "    &= \\frac{1}{12}m\\left(l^2 + t^2\\right) + m\\left[\\left(\\frac{t}{2}\\right)^2 + \\left(r\\theta\\right)^2\\right]\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "The total kinetic energy is then:\n",
    "\n",
    "$$\n",
    "T = \\frac{m\\dot{\\theta}^2t^2}{24} + \\frac{m\\dot{\\theta}^2l^2}{24} + \\frac{m\\dot{\\theta}^2t^2}{8} + \\frac{m\\dot{\\theta}^2r^2\\theta^2}{2}\n",
    "$$\n",
    "\n",
    "We can now see how both the linear and rotational motion of the center of mass is represented in this kinetic energy, and the parallel axis theorem has done the hard work for us.\n",
    "\n",
    "The potential energy comes from the center of mass of the\n",
    "book rising as the book rotates. We can solve for the vertical position, referenced from the center of the cup, then subtract out the vertical position with $\\theta=0$ to obtain the change in height.\n",
    "\n",
    "$$\n",
    "y_O(\\theta) = r\\cos\\theta + r\\theta\\sin\\theta + \\frac{t}{2}\\cos\\theta\n",
    "$$\n",
    "\n",
    "So now the change in height is\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\Delta y_O &= y_O(\\theta) - y_O(0) \\\\\n",
    "           &= \\left(\\cos\\theta - 1\\right)\\left( r + \\frac{t}{2}\\right) + r\\theta\\sin\\theta\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "The potential energy then is\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "V &= mg\\Delta y_O \\\\\n",
    "  &= mg\\left[\\left(\\cos\\theta - 1\\right)\\left( r + \\frac{t}{2}\\right) + r\\theta\\sin\\theta\\right]\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Now taking derivatives of the Lagrangian $L = T - V$, we get\n",
    "\n",
    "$$\n",
    "\\frac{\\partial L}{\\partial\\dot{\\theta}} = I_C\\dot{\\theta}\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\frac{d}{dt}\\left(\\frac{\\partial L}{\\partial\\dot{\\theta}}\\right) = I_C \\ddot{\\theta}\n",
    "$$\n",
    "\n",
    "$$\n",
    "\\frac{\\partial L}{\\partial \\theta} = m\\dot{\\theta}^2r^2\\theta - mgr\\theta\\cos\\theta + mg\\frac{t}{2}\\sin\\theta\n",
    "$$\n",
    "\n",
    "So our equation of motion is\n",
    "\n",
    "$$\n",
    "m\\left(\\frac{t}{3}^2 + \\frac{l^2}{12} + r^2\\theta^2\\right) \\ddot{\\theta} + mr^2\\theta\\dot{\\theta}^2 + mgr\\theta\\cos\\theta - mg\\frac{t}{2}\\sin\\theta = 0\n",
    "$$\n",
    "\n",
    "**Linearized Equation of Motion**\n",
    "\n",
    "We'll linearize this using the small angle approximation. Specifically, let $\\sin\\theta \\approx \\theta$, $\\cos\\theta \\approx 1$ (note this is not a particularly good approximation), $\\theta^2 \\approx 0$, and $\\theta\\dot{\\theta}^2 \\approx 0$. These are not necessarily unreasonable assumptions, but some are not all that accurate.\n",
    "\n",
    "$$\n",
    "m\\left(\\frac{t^2}{3} + \\frac{l^2}{12}\\right)\\ddot{\\theta} + mg\\left(r - \\frac{t}{2}\\right)\\theta = 0\n",
    "$$\n",
    "\n",
    "Interestingly, the mass drops out and the linearized system does not depend on the mass of the book, only its dimensions.\n",
    "\n",
    "$$\n",
    "\\left(\\frac{t^2}{3} + \\frac{l^2}{12}\\right)\\ddot{\\theta} + g\\left(r - \\frac{t}{2}\\right)\\theta = 0\n",
    "$$\n",
    "\n",
    "**Stability**\n",
    "\n",
    "For stability, we only require that the coefficient of $\\theta$ in the equation of motion (i.e. the system stiffness) is positive:\n",
    "\n",
    "$$\n",
    "r > \\frac{t}{2}\n",
    "$$\n",
    "\n",
    "This is a bit counterintuitive in that one would think the length of the book should be taken into account. For example, if the book is very short and very thick, it would tend to fall over if perturbed. It turns out, however, that even for a very short book, the cup radius is what really matters. The only requirement for stability is that the center of mass always remains \"inside\" the contact point (i.e. closer horizontally to the center of the cup) so that the moment imparted about the contact point by gravity restores the book toward the equilibrium $\\theta = 0$. Recall from previous homework that the length of the book has nothing to do with the horizontal location of the contact point--it only affects the inertia and hence the period of oscillation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "nbgrader": {
     "grade": false,
     "grade_id": "cell-04c2052b033caf47",
     "locked": true,
     "schema_version": 1,
     "solution": false
    }
   },
   "source": [
    "# Problem 3\n",
    "\n",
    "Outside of a small range of deflections, springs tend to behave nonlinearly. For example, a spring may behave somewhat linearly for $\\pm 1~\\textrm{cm}$, but as it is stretched more and more, the force becomes effectively infinite (at some point, the material would start to yield, so it's not truly infinite). One potential model of this kind of behavior is a tangent function.\n",
    "\n",
    "Generally, the shape of the curve $a \\tan bx$ is similar to what we want: it has a roughly linear region for small values of $x$ and it rapidly increases as $x$ approaches a limiting value. It'd be nice to be able to parameterize this spring characteristic in terms of things we can more directly relate to.\n",
    "\n",
    "Determine the stiffness characteristic $F(x) = a \\tan bx$, but instead of $a$ and $b$, use a linear stiffness parameter $k$ (i.e. the slope at $x = 0$) and bounds $\\pm x_\\mathrm{lim}$ (i.e. determine what $a$ and $b$ should be in terms of $k$ and $x_\\mathrm{lim}$).\n",
    "\n",
    "Use $k = 1000~\\mathrm{N/m}$ and $x_\\mathrm{lim} = 5~\\mathrm{cm}$ and plot the spring characteristic $F(x)$ curve.\n",
    "\n",
    "Use a `SingleDoFNonLinearSystem` to simulate a simple mass-spring system with the equation of motion below. Use mass $m = 1~\\mathrm{kg}$ and an initial position of $x = 2.5~\\mathrm{cm}$.\n",
    "\n",
    "$$\n",
    "m\\ddot{x} + F(x) = 0\n",
    "$$\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbgrader": {
     "grade": true,
     "grade_id": "problem-03",
     "locked": false,
     "points": 5,
     "schema_version": 1,
     "solution": true
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function $a \\tan bx$ is relevant here for values of $x$ between $\\pm \\frac{\\pi}{2b}$, so $b = \\pm \\frac{\\pi}{2x_\\mathrm{lim}}$. The slope of the curve at $x = 0$ is $ab$, so $a = \\frac{k}{b}$.\n",
    "\n",
    "We'll use these relationships to write a function that takes an array of positions, a linear stiffness value $k$, and a limit $x_\\mathrm{lim}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def spring_force(x, k, xlim):\n",
    "    # limits of tan(b*x) is pi/(2*b)\n",
    "    b = np.pi / 2 / xlim\n",
    "    # slope of a*tan(b*x) is a*b\n",
    "    a = k / b\n",
    "    F = a * np.tan(b * x)\n",
    "    return F"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the values given for $x_\\mathrm{lim}$ and $k$, we can plot the spring characteristic."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xlim = 0.05\n",
    "k = 1000\n",
    "x = np.arange(-0.9*xlim, 0.9*xlim, 0.001)\n",
    "F = spring_force(x, k, xlim)\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(x, F)\n",
    "ax.set_xlabel('deflection (m)')\n",
    "ax.set_ylabel('force (N)')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To simulate a system with the given equation of motion, we'll use a `SingleDoFNonLinearSystem` and write the `diff_eq_func`. To do this, we need to write the equation of motion as a system of first order ODEs. The second order equation of motion is\n",
    "\n",
    "$$\n",
    "m\\ddot{x} + F(x) = 0\n",
    "$$\n",
    "\n",
    "Letting $q = x$ and $p = \\dot{x}$, we get the following equations:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\dot{q} &= p\\\\\n",
    "\\dot{p} &= \\ddot{x} = -\\frac{F(x)}{m}\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Translating this to code, we get the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def rhs(x, v, m, k, xlim):\n",
    "    xd = v\n",
    "    vd = -spring_force(x, k, xlim) / m\n",
    "    return xd, vd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can set up the system, giving it the appropriate constants and initial position and speed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from resonance.nonlinear_systems import SingleDoFNonLinearSystem\n",
    "\n",
    "sys = SingleDoFNonLinearSystem()\n",
    "sys.constants['m'] = 1\n",
    "sys.constants['k'] = 1000\n",
    "sys.constants['xlim'] = 0.05\n",
    "sys.coordinates['x'] = 0.025\n",
    "sys.speeds['v'] = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can set the `diff_eq_func` for the system to the function we wrote implementing the first order ODEs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sys.diff_eq_func = rhs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now simulate the free response and plot the position in time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "traj = sys.free_response(1, sample_rate=500)\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(traj.index, traj.x)\n",
    "ax.set_xlabel('time (s)')\n",
    "ax.set_ylabel('position (m)')"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Create Assignment",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
