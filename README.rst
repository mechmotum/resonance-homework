============================
Resonance Homework Notebooks
============================

This repository contains Jupyter notebooks that are homework companions to
resonance_, a set of notebooks and a library for teaching undergraduate
mechanical vibrations.

The notebooks are made to work with nbgrader_, a tool for handling homework
notebooks. This just means the notebooks have some cells with metadata fields
indicating they are for students to write their code or text to answer the
questions.


Environment
===========

Follow the instructions for creating a dev environment at resonance_, then
install nbgrader with::

    $ conda install nbgrader


Creating Homework
=================

Homework Notebook Style
-----------------------

When creating a new homework assignment, you write the the problem prompts and
the solutions all at once.

For each problem, use a single cell to write the problem description. Add
a horizontal line at the bottom of the cell (i.e. using three or more hyphens).
This is helpful when looking at a complete notebook to see where the solution
begins. Mark the problem prompt as a read-only cell. Just below the prompt, add
a manual grading cell. Give the cell a useful cell ID (e.g. "p1") and set the
number of points that problem is worth.

In general, any cell not marked as a specific type of cell is stripped out when
generating the version of the notebook to put on the server. That is, the dropdown
when using the "Create Assignemnt" cell toolbar should be blank. Use as many
cells as you see fit to solve the problem, using the style you expect from the
students, and leave them all unmarked.

Creating the Student Version
----------------------------

Before uploading the notebook to the server, you need to strip out the
solutions. The script ``assign.py`` does this. It copies the specified notebook
over to the ``build`` folder and strips out any non-nbgrader cells, leaving the
question prompts and grading cells.

Releasing the Assignment
------------------------

Create an assignment through the formgrader, then upload the output of
``assign.py`` as well as any images needed. Use ``nbgrader assign``, preview
the student version, and then ``nbgrader relesase``.


Make Solutions Files
--------------------

The script ``make-solution.py`` takes in the homework solutions in ``source/``
and generates another ipynb file with a copyright header as well as a PDF
version via LaTex. It is run as::

    $ ./make-solution.py source/hw01.ipynb

This generates files in a ``build/`` folder.


Grading the Homework
====================

On the server, open a terminal and use ``nbgrader collect`` and ``nbgrader
autograde`` to collect and execute the student submitted notebooks. Then go the
formgrader tab and go to the manual grading page to navigate to the assignment.
When you open a student notebook to grade, you are presented with an HTML
rendering of the notebook. You can leave feedback for each problem and provide
a score for each problem using this interface.

If you want to provide more localized feedback, use the link at the top (e.g.
"Submission #1") to open the actual notebook file and insert cells to add
comments. Use HTML to make the text stand out when the student reviews the
feedback, for example:

.. code:: html

   <font size="5" color="green">Great job!</font>

When you're done, save the notebook. Upon refreshing the HTML view of the
notebook, the additional cells should show up.

Generating Feedback
-------------------

To generate feedback from the manual grading step, use ``nbgrader feedback``.
This will generate HTML files that you can then distribute to students. It
should contain your nbgrader comments (entered through the HTML grading
interface) as well as the additional cells you add for more localized comments.

::

   nbgrader feedback <assignment name>

Distribution
------------

The feedback HTML files can be automatically distributed to the student's home
directories. This is done with a custom script that is on the eng122 server
account called ``return_feedback.py``. Use it like::

   python return_feedback.py <assignment name>

The PDF solution should be uploaded to Canvas.

.. _resonance: https://github.com/moorepants/resonance
.. _nbgrader: https://nbgrader.readthedocs.io/en/stable/
