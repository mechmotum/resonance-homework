#!/usr/bin/env python
"""

Usage::

    python make_solution.py source/hw_01.ipynb

"""

import sys
import os
import glob
import subprocess
from shutil import copyfile
import nbformat
from nbformat.v4.nbbase import new_markdown_cell
from nbconvert.preprocessors import ExecutePreprocessor, Preprocessor


sourcedir = 'source'
destdir = 'build'
copyright_template = (
    "# Homework Solutions\n\n"
    "These solutions are copyrighted by Kenneth R. Lyons and Jason K. Moore © "
    "2017. Redistribution outside of the current class is prohibited."
)


class SVGToPDFPreprocessor(Preprocessor):
    """Search for `.svg` in markdown cells and replace with `.png`."""

    def preprocess_cell(self, cell, resources, index):
        if cell['cell_type'] == 'markdown':
            cell['source'] = cell['source'].replace('.svg', '.png')
        return cell, resources


def convert_svg(fp):
    """Use Inkscape to convert SVGs to PNGs."""
    _, fn = os.path.split(fp)
    name, ext = os.path.splitext(fn)
    outfp = os.path.join(destdir, name + ".png")
    subprocess.call(["inkscape", f, "--export-png={}".format(outfp)])


def make_pdf(fp):
    """Use nbconvert to make a tex file and compile with xelatex."""
    # TODO use nbformat/nbformat APIs instead; too lazy at the moment
    nbdir, nbfile = os.path.split(fp)
    name, _ = os.path.splitext(nbfile)
    texfile = "{}.tex".format(name)
    cwd = os.getcwd()
    os.chdir(nbdir)
    subprocess.call(["jupyter-nbconvert", nbfile, "--to", "latex"])
    subprocess.call(["xelatex", texfile])
    os.chdir(cwd)


def process_notebook(fp):
    # parse out file info
    nbdir, nbfile = os.path.split(fp)
    nbname, ext = os.path.splitext(nbfile)
    outfile = "{}_solutions{}".format(nbname, ext)
    outfp = os.path.join(destdir, outfile)

    nb = nbformat.read(fp, as_version=4)

    # insert copyright cell at the top
    nb.cells.insert(0, new_markdown_cell(copyright_template))

    resources = {'metadata': {'path': destdir}}

    svgp = SVGToPDFPreprocessor()
    svgp.preprocess(nb, resources)

    # execute the notebook
    ep = ExecutePreprocessor()
    ep.preprocess(nb, resources)

    nbformat.write(nb, outfp)

    make_pdf(outfp)


if __name__ == '__main__':
    files = sys.argv[1:]

    os.makedirs(destdir, exist_ok=True)

    # make sure extra files are in destdir
    for f in glob.glob(os.path.join(sourcedir, "*.svg")):
        convert_svg(f)

    for typ in ["*.png", "*.jpg", "*.csv"]:
        for f in glob.glob(os.path.join(sourcedir, typ)):
            copyfile(f, os.path.join(destdir, os.path.basename(f)))

    for f in files:
        process_notebook(f)
