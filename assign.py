#!/usr/bin/env python
"""Create the student version of the assignment.

The notebooks are in their full solution form, and this script can strip out
the solutions.
"""

import sys
import os
import nbformat


sourcedir = 'source'
destdir = 'build'


def check_locked(cell):
    """Checks if the cell is an nbgrader cell."""
    return 'nbgrader' in cell.metadata


def process_notebook(fp):
    # parse out file info
    nbdir, nbfile = os.path.split(fp)
    nbname, ext = os.path.splitext(nbfile)
    outfile = "{}_student{}".format(nbname, ext)
    outfp = os.path.join(destdir, outfile)

    nb = nbformat.read(fp, as_version=4)

    nb['cells'][:] = [cell for cell in nb.cells if check_locked(cell)]

    nbformat.write(nb, outfp)


if __name__ == '__main__':
    files = sys.argv[1:]

    os.makedirs(destdir, exist_ok=True)

    for f in files:
        process_notebook(f)
